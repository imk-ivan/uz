﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UZ.DAL.Models;

namespace UZ.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserClaim<string>
    , ApplicationUserRole, IdentityUserLogin<string>, IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
        }

        public DbSet<TrainSearch> TrainSearches { get; set; }
        public DbSet<WagonType> WagonTypes { get; set; }
        public DbSet<ReservedTicket> ReservedTickets { get; set; }
        public DbSet<BlockedTrain> BlockedTrains { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>()
                .HasMany(e => e.UserRoles)
                .WithOne()
                .HasForeignKey(e => e.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<ApplicationUserRole>()
                .HasOne(e => e.User)
                .WithMany(e => e.UserRoles)
                .HasForeignKey(e => e.UserId);

            builder.Entity<ApplicationUserRole>()
                .HasOne(e => e.Role)
                .WithMany(e => e.UserRoles)
                .HasForeignKey(e => e.RoleId);

            builder.Entity<TrainSearchesWagonType>()
            .HasKey(t => new { t.TrainSearchId, t.WagonTypeId });

            builder.Entity<TrainSearchesWagonType>()
                .HasOne(pt => pt.WagonType)
                .WithMany(p => p.TrainSearchesWagonTypes)
                .HasForeignKey(pt => pt.WagonTypeId);

            builder.Entity<TrainSearchesWagonType>()
                .HasOne(pt => pt.TrainSearch)
                .WithMany(t => t.TrainSearchesWagonTypes)
                .HasForeignKey(pt => pt.TrainSearchId);

        }
    }
}
