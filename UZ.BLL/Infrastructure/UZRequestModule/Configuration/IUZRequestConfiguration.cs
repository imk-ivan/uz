﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Infrastructure.UZRequestModule.Configuration
{
    public interface IUZRequestConfiguration
    {
        string API_BASE { get; }
        string API_PAY_BASE {get;}
    }
}
