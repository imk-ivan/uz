﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TicketSearcher.LinkGeneration
{
    public static class LinkGenerator
    {
        public static string GenerateAdminTicketLink(string apiBase, int ticketId)
        {
            string link = $"{apiBase}/admin/ticket/{ticketId}";
            return link;
        }
    }
}
