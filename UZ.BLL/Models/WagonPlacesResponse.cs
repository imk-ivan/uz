﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Models
{
    public class WagonPlacesResponse
    {
        public WagonPlacesResponseData Data { get; set; }
    }

    public class WagonPlacesResponseData
    {
        public Dictionary<string, List<int>> Places { get; set; }
        public string Scheme { get; set; }

        private WagonPlacesResponseScheme _wagonScheme;
        
        public WagonPlacesResponseScheme WagonScheme
        {
            get
            {
                if(_wagonScheme == null)
                {
                    _wagonScheme = Newtonsoft.Json.JsonConvert.DeserializeObject<WagonPlacesResponseScheme>(Scheme);
                }
                return _wagonScheme;
            }
        }
    }    

    public class WagonPlacesResponseScheme
    {
        public string Scheme_type { get; set; }
        public WagonPlacesResponseSchemeModel Model { get; set; }
        public WagonPlacesResponseSchemePlaces Places { get; set; }
    }

    public class WagonPlacesResponseSchemeModel
    {
        public Dictionary<string, WagonPlacesResponseSchemeModelFloor> Floor { get; set; }
    }
    public class WagonPlacesResponseSchemeModelFloor
    {
        public int? Width { get; set; }
        public int? Height { get; set; }
    }
    public class WagonPlacesResponseSchemePlaces
    {
        public Dictionary<string, List<WagonPlacesResponseSchemePlacesFloor>> Floor { get; set; }
    }
    public class WagonPlacesResponseSchemePlacesFloor
    {
        public int? Y { get; set; }
        public int? X { get; set; }
        public int? W { get; set; }
        public int? H { get; set; }
        public string Num { get; set; }
        public string Pos { get; set; }
        public string Up { get; set; }
        public string Type { get; set; }
    }


    

}
