﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UZ.BLL.Models;

namespace UZ.Web.Models.SearchViewModels
{
    public class SearchResultViewModel
    {
        public AdvancedTrainSearchDTO Search { get; set; }
        public List<TrainSearchData> Trains { get; set; }
    }
}
