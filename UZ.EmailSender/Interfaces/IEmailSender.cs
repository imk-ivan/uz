﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UZ.EmailSender.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message, bool isBodyHtml = false);
        void SendEmail(string email, string subject, string message, bool isBodyHtml = false);
    }
}
