﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class TicketDateAndRebookingCounter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReservedTickets_TrainSearches_TrainSearchId",
                table: "ReservedTickets");

            migrationBuilder.AlterColumn<int>(
                name: "TrainSearchId",
                table: "ReservedTickets",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "ReservedTickets",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true,
                defaultValue: DateTime.Now);

            migrationBuilder.AddColumn<int>(
                name: "RebookedCounter",
                table: "ReservedTickets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_ReservedTickets_TrainSearches_TrainSearchId",
                table: "ReservedTickets",
                column: "TrainSearchId",
                principalTable: "TrainSearches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReservedTickets_TrainSearches_TrainSearchId",
                table: "ReservedTickets");

            migrationBuilder.DropColumn(
                name: "RebookedCounter",
                table: "ReservedTickets");

            migrationBuilder.AlterColumn<int>(
                name: "TrainSearchId",
                table: "ReservedTickets",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Date",
                table: "ReservedTickets",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddForeignKey(
                name: "FK_ReservedTickets_TrainSearches_TrainSearchId",
                table: "ReservedTickets",
                column: "TrainSearchId",
                principalTable: "TrainSearches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
