﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using UZ.Enums;

namespace UZ.BLL.Infrastructure.Proxy
{
    public class ProxyConfig
    {
        public readonly string PROXY_LOGIN = "ikolpi";
        public readonly string PROXY_PASSWORD = "HGvm4ZMD";
        public ICredentials Credentials;
        private ProxyUsingType ProxyUsingType;
        private int ProxyIndex  = 0;
        Random rnd;

        private static readonly Object lockObj = new Object();

        public List<string> addresses;

        public ProxyConfig(ProxyUsingType usingType = ProxyUsingType.Random)
        {
            addresses = new List<string>() { "91.108.64.141:29842" };
            ProxyUsingType = usingType;
            Credentials = GetCredentials();
            rnd = new Random();
        }

        public ProxyConfig(string login, string password, List<string> proxyAddresses, ProxyUsingType usingType = ProxyUsingType.Random)
        {
            addresses = proxyAddresses;
            PROXY_LOGIN = login;
            PROXY_PASSWORD = password;
            Credentials = GetCredentials();
            rnd = new Random();
            ProxyUsingType = usingType;
        }

        public string GetAddress()
        {
            lock (lockObj)
            {
                if (ProxyIndex == addresses.Count)
                {
                    ProxyIndex = 0;
                }
                int addressNumber = ProxyUsingType == ProxyUsingType.Random ? rnd.Next(addresses.Count) : ProxyIndex++;

                return addresses[addressNumber];
            }
            
        }

        private ICredentials GetCredentials() => new NetworkCredential(PROXY_LOGIN, PROXY_PASSWORD);
    }
}
