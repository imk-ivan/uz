﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class AddSearchStationTitles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StationFrom",
                table: "TrainSearches",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StationTo",
                table: "TrainSearches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StationFrom",
                table: "TrainSearches");

            migrationBuilder.DropColumn(
                name: "StationTo",
                table: "TrainSearches");
        }
    }
}
