﻿using System;
using System.Collections.Generic;
using System.Text;
using UZ.BLL.Models;

namespace UZ.BLL.Extensions
{
    public static class TicketDTOExtensions
    {
        public static void SetDates(this TicketDTO ticket, string dateFrom, string timeFrom, string dateTo, string timeTo)
        {
            ticket.Date = DateTime.Parse(dateFrom);
            TimeSpan departureTime = TimeSpan.Parse(timeFrom);
            ticket.Date = ticket.Date.Add(departureTime);

            ticket.ArrivalDate = DateTime.Parse(dateTo);
            TimeSpan arrivalTime = TimeSpan.Parse(timeTo);
            ticket.ArrivalDate = ticket.ArrivalDate.Add(arrivalTime);
        }
    }
}
