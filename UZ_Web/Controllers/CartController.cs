﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Models;
using UZ.BLL.Utils;

namespace UZ.Web.Controllers
{
    [Route("cart")]
    public class CartController : BaseController
    {
        public CartController(IUZRequestService _uzService)
            : base(_uzService)
        {

        }

        [Route("add")]
        public IActionResult Index()
        {
            TicketDTO ticket = new TicketDTO()
            {
                Ord = 0,
                From = 2210670,
                To = 2204001,
                Train = "082П",
                //Date = "2018-09-07",
                WagonNumber = 6,
                WagonClass = "В",
                WagonType = "П",
                WagonRailway = 43,
                Charline = "Б",
                FirstName = "Геннадий",
                LastName = "Сараула",
                Bedding = 1,
                Services = new string[] {"Н"},
                Child = "",
                Student = "",
                Reserve = 0,
                PlaceNumber = 10
            };

            TicketDTO secondTicket = new TicketDTO()
            {
                Ord = 0,
                From = 2204001,
                To = 2208001,
                Train = "059О",
                //Date = "2018-08-24",
                WagonNumber = 9,
                WagonClass = "Б",
                WagonType = "П",
                WagonRailway = 40,
                Charline = "А",
                FirstName = "Татьяна",
                LastName = "Остапчук",
                Bedding = 1,
                Services = new string[] { "Н", "М" },
                Child = "",
                Student = "",
                Reserve = 0,
                PlaceNumber = 47
            };

            OrderDto order = new OrderDto(new List<TicketDTO>() { ticket });

            TicketResponse result = UZRequestService.AddTicketToCart(order, out string sessionId);
            string content = "";
            if (result != null && result.Error == 0)
            {
                //string cartHtml = UZRequestService.GetCart(sessionId);
                //List<string> reserveIds = StringUtils.GetReserveIdFromCartHtml(cartHtml);

                //string revokeResult = UZRequestService.RevokeTicketFromCart(sessionId, reserveIds.ToArray());

            }
            else if(result != null && result.Captcha == 1)
            {
                byte[] img = UZRequestService.GetTicketCaptchaImage(sessionId);
                return File(img, "image/gif");
            }


            return Content(content);
        }
    }
}