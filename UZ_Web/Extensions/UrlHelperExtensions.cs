using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UZ.Web.Areas.Admin.Controllers;
using UZ.Web.Controllers;

namespace Microsoft.AspNetCore.Mvc
{
    public static class UrlHelperExtensions
    {
        public static string EmailConfirmationLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.ConfirmEmail),
                controller: "Account",
                values: new { userId, code },
                protocol: scheme);
        }

        public static string ResetPasswordCallbackLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.ResetPassword),
                controller: "Account",
                values: new { userId, code },
                protocol: scheme);
        }

        public static string TrainSearchUnsubscribeLink(this IUrlHelper urlHelper, string searchIdString, string scheme)
        {
            return urlHelper.Action(
                action: nameof(TrainController.UnsubscribeTrainSearch),
                controller: "Train",
                values: new { searchIdString },
                protocol: scheme);
        }

        public static string AdminTicketLink(this IUrlHelper urlHelper, int id, string scheme)
        {
            return urlHelper.Action(
                action: nameof(TicketController.Detail),
                controller: "Ticket",
                values: new { id },
                protocol: scheme);
        }
    }
}
