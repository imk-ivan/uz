﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Infrastructure.UZRequestModule.Configuration
{
    public class UZRequestConfiguration : IUZRequestConfiguration
    {
        public string API_BASE { get; set; }
        public string API_PAY_BASE { get; set; }
    }
}
