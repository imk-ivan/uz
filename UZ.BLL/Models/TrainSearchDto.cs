﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using UZ.Enums;

namespace UZ.BLL.Models
{
    public class BaseTrainSearchDTO
    {
        public int Id { get; set; }

        [DisplayName("Откуда")]
        [Required(ErrorMessage = "Выберите пункт отправления")]
        public int From { get; set; }

        public string StationFrom { get; set; }

        [DisplayName("Куда")]
        [Required(ErrorMessage = "Выберите пункт назначения")]
        public int To { get; set; }

        public string StationTo { get; set; }

        [DisplayName("Дата")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Укажите дату отправления")]
        public DateTime Date { get; set; }

        public string UserId { get; set; }

        [DisplayName("Активен")]
        public bool Active { get; set; }
    }

    public class TrainSearchDTO : BaseTrainSearchDTO
    {
              
    }

    public class AdvancedTrainSearchDTO : BaseTrainSearchDTO
    {
        [MaxLength(25, ErrorMessage = "Максимальная длина {1} символов")]
        [Required(ErrorMessage = "Введите Имя")]
        [DisplayName("Имя")]
        public string FirstName { get; set; }

        [MaxLength(25, ErrorMessage = "Максимальная длина {1} символов")]
        [Required(ErrorMessage = "Введите Фамилию")]
        [DisplayName("Фамилия")]
        public string LastName { get; set; }

        [MaxLength(50, ErrorMessage = "Максимальная длина {1} символов")]
        [Required(ErrorMessage = "Введите Email")]
        [DisplayName("Email")]
        public string Email { get; set; }

        
        private string _phone;

        [MaxLength(25, ErrorMessage = "Максимальная длина {1} символов")]
        [Required(ErrorMessage = "Введите телефонный номер")]
        [DisplayName("Телефон")]
        public string Phone
        {
            get
            {
                return this._phone;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _phone = Regex.Replace(value, @"\D", "");
                }
            }
        }



        [Required(ErrorMessage = "Укажите тип вагона")]
        [DisplayName("Тип вагона")]
        public List<WagonTypeDto> WagonTypes { get; set; }

        [DisplayName("Только нижние")]
        public bool BottomOnly { get; set; }

        [DisplayName("С бельем")]
        public bool Bedding { get; set; }

        [Required]
        public List<string> TrainsIds { get; set; }

        public TrainPriority TrainPriority { get; set; }
    }
}
