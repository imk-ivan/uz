﻿var CustomDateFilter = (function (base) {
    MvcGridExtends(CustomDateFilter, base);

    function CustomDateFilter() {
        base.apply(this);
    }
    
    CustomDateFilter.prototype.bindValue = function (grid, column, popup) {
        popup.find('.mvc-grid-value').datepicker({ dateFormat: 'yy-mm-dd' });

        base.prototype.bindValue.call(this, grid, column, popup);
    };

    return CustomDateFilter;
})(MvcGridDateFilter);

var CustomBoolFilter = (function (base) {
    MvcGridExtends(CustomBoolFilter, base);

    function CustomBoolFilter() {
        base.apply(this);

        this.methods = ['equals', 'not-equals'];
    }

    // Other extension points can be found at MvcGridFilter declaration
    CustomBoolFilter.prototype.renderFilter = function (grid, filter) {
        var lang = { 'equals': 'Соответствует', 'not-equals': 'Не соответствует' };

        return base.prototype.renderFilter.call(this, grid, filter, lang);
    };

    return CustomBoolFilter;
})(MvcGridFilter);

$('.mvc-grid').mvcgrid({
    filters: {
        'date': new CustomDateFilter(),
        'boolFilter': new CustomBoolFilter()
    }
});