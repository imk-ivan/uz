﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UZ.BLL.Models
{
    public class BlockedTrainDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Введите номер поезда")]
        [MaxLength(25, ErrorMessage = "Максимальная длина {1} символов")]
        [Display(Name = "Номер поезда")]
        public string TrainId { get; set; }

        public DateTime BlockedDate { get; set; }
    }
}
