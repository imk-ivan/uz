﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.DAL.Models
{
    public class ApplicationUser : IdentityUser
    {
        public virtual List<TrainSearch> TrainSearches { get; set; }

        public virtual ICollection<ApplicationUserRole> UserRoles { get; } = new List<ApplicationUserRole>();
    }
}
