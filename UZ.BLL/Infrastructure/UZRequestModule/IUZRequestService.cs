﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Text;
using UZ.BLL.Models;

namespace UZ.BLL.Infrastructure.UZRequestModule
{
    public interface IUZRequestService
    {
        void InitializeProxy(ICredentials credentials, string proxyAddress);
        List<StationDTO> GetStations(string term, int stationCount = 5);
        string GetCart(string sessionId);
        RevokeResponse RevokeTicketFromCart(string sessionId, string[] ids);
        TicketResponse AddTicketToCart(OrderDto order, out string sessionId, int? captcha = null);
        string PayCart(string sessionId, string email);
        string RedirectToPay(string sessionId, int merchant_id, int order_id, int amount, string date, string description, string sd, string version, string signature, int enable_mobile);
        List<TrainSearchData> TrainSearch(BaseTrainSearchDTO trainSearch);
        List<TrainSearchData> TrainSearch(TrainSearchDTO trainSearch);
        List<TrainSearchData> TrainSearch(AdvancedTrainSearchDTO trainSearch);
        TrainWagonsResponse GetTrainWagons(int from, int to, string date, string train, string wagonTypeId, int getTpl = 1);
        WagonPlacesResponse GetWagonPlaces(int from, int to, string date, string train, string wagonTypeId, int wagonNum, string wagonClass);
        byte[] GetTicketCaptchaImage(string sessionId);
    }
}
