﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class TicketSearchKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TrainSearchId",
                table: "ReservedTickets",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ReservedTickets_TrainSearchId",
                table: "ReservedTickets",
                column: "TrainSearchId");

            migrationBuilder.AddForeignKey(
                name: "FK_ReservedTickets_TrainSearches_TrainSearchId",
                table: "ReservedTickets",
                column: "TrainSearchId",
                principalTable: "TrainSearches",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReservedTickets_TrainSearches_TrainSearchId",
                table: "ReservedTickets");

            migrationBuilder.DropIndex(
                name: "IX_ReservedTickets_TrainSearchId",
                table: "ReservedTickets");

            migrationBuilder.DropColumn(
                name: "TrainSearchId",
                table: "ReservedTickets");
        }
    }
}
