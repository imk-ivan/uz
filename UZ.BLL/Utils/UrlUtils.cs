﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using UZ.BLL.Models;

namespace UZ.BLL.Utils
{
    public static class UrlUtils
    {
        public static string GetTrainWagonsUrl(TrainSearchData train, string wagonType="П")
        {
            string result = "";
            try
            {
                result = $"from={train.From.Code}&to={train.To.Code}&date={train.From.SrcDate}&time=00:00&train={train.Num}&wagon_type_id={wagonType}&url=train-wagons";
                //result = HttpUtility.UrlEncode(result);
            }
            catch(Exception ex) { }
            
            return result;
        }

        public static string GetTrainWagonsUrl(AdvancedTrainSearchDTO search, string trainId, string wagonType = "П")
        {
            string result = "";
            try
            {
                result = $"https://booking.uz.gov.ua/?from={search.From}&to={search.To}&date={search.Date.ToString("yyyy-MM-dd")}&time=00:00&train={trainId}&wagon_type_id={wagonType}&url=train-wagons";                
            }
            catch (Exception ex) { }

            return result;
        }
    }
}
