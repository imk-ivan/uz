﻿$(function () {
    $(document).on('change', ".ticket-status input[type='checkbox']", function (e) {

        var activeStatus = $(this).is(':checked');
        var ticketId = $(this).parents('.ticket-status').data('ticket-id');

        $.ajax({
            url: '/admin/ticket/set-ticket-status',
            data: { ticketId: ticketId, status: activeStatus },
            type: 'POST',
            success: function (data) {
                $("#Notifications").html(data).addClass("show");
            }
        });

    }).on('click', '.copy-text', function () {
        $("#sessionId").selectText();
        document.execCommand("copy");

        window.getSelection().removeAllRanges();
        var tooltip = $('.tooltiptext', $(this))[0];
        tooltip.innerHTML = "Скопировано в буфер обмена";
        })
        .on('mouseout', '.copy-text', function () {
            var tooltip = $('.tooltiptext', $(this))[0];
            tooltip.innerHTML = "Копировать";
        })
})

jQuery.fn.selectText = function () {
    var doc = document;
    var element = this[0];
    if (doc.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
};