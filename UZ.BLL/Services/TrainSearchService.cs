﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.DAL;
using UZ.DAL.Models;
using UZ.Enums;

namespace UZ.BLL.Services
{
    public class TrainSearchService : ITrainSearchService
    {
        private ApplicationDbContext db;

        private readonly IMapper mapper;

        public TrainSearchService(ApplicationDbContext _db, IMapper _mapper)
        {
            db = _db;
            mapper = _mapper;
        }

        public DbSet<TrainSearch> Storage
        {
            get
            {
                return db.TrainSearches;
            }
        }

        public async Task<T> GetAsync<T>(int id)
            where T : BaseTrainSearchDTO
        {
            TrainSearch search = await db.TrainSearches.Include(trainSearch=>trainSearch.TrainSearchesWagonTypes).FirstOrDefaultAsync(trainSearch=>trainSearch.Id== id);
            if(search != null)
            {
                return mapper.Map<T>(search);
            }
            return null;
        }

        public async Task<List<T>> GetAllSearchesAsync<T>(DateTime? date = null, bool onlyActive = true)
            where T : BaseTrainSearchDTO
        {
            try
            {
                IQueryable<TrainSearch> sourceQuery = db.TrainSearches.AsQueryable();
                if (date != null)
                    sourceQuery = sourceQuery.Where(ts => ts.Date.Date >= date);
                if (onlyActive)
                    sourceQuery = sourceQuery.Where(ts => ts.Active == true);
                var source = await sourceQuery.Include(x=>x.TrainSearchesWagonTypes).ThenInclude(twt=>twt.WagonType).ToListAsync();
                return mapper.Map<List<TrainSearch>, List<T>>(source);
            }
            catch (Exception ex)
            {
                return new List<T>();
            }
        }

        public async Task<List<T>> GetAllSearchesAsync<T>(string query, SortState sortOrder = SortState.DateDesc)
            where T : BaseTrainSearchDTO
        {
            try
            {
                IQueryable<TrainSearch> sourceQuery = db.TrainSearches.AsQueryable();

                if (String.IsNullOrEmpty(query))
                    sourceQuery = sourceQuery.Where(ts => ts.StationFrom.Contains(query) 
                    || ts.StationTo.Contains(query) 
                    || ts.FirstName.Contains(query)
                    || ts.LastName.Contains(query)
                    || ts.Email.Contains(query));

                switch (sortOrder)
                {
                    case SortState.DateAsc:
                        sourceQuery = sourceQuery.OrderBy(ts => ts.Date);
                        break;
                    case SortState.DateDesc:
                        sourceQuery = sourceQuery.OrderByDescending(ts => ts.Date);
                        break;
                    case SortState.EmailAsc:
                        sourceQuery = sourceQuery.OrderBy(ts => ts.Email);
                        break;
                    case SortState.EmailDesc:
                        sourceQuery = sourceQuery.OrderByDescending(ts => ts.Email);
                        break;
                    case SortState.FirstNameAsc:
                        sourceQuery = sourceQuery.OrderBy(ts => ts.FirstName);
                        break;
                    case SortState.FirstNameDesc:
                        sourceQuery = sourceQuery.OrderByDescending(ts => ts.FirstName);
                        break;
                    case SortState.LastNameAsc:
                        sourceQuery = sourceQuery.OrderBy(ts => ts.LastName);
                        break;
                    case SortState.LastNameDesc:
                        sourceQuery = sourceQuery.OrderByDescending(ts => ts.LastName);
                        break;
                    case SortState.FromAsc:
                        sourceQuery = sourceQuery.OrderBy(ts => ts.From);
                        break;
                    case SortState.FromDesc:
                        sourceQuery = sourceQuery.OrderByDescending(ts => ts.From);
                        break;
                    case SortState.ToAsc:
                        sourceQuery = sourceQuery.OrderBy(ts => ts.To);
                        break;
                    case SortState.ToDesc:
                        sourceQuery = sourceQuery.OrderByDescending(ts => ts.To);
                        break;
                    default:
                        break;
                }

                var source = await sourceQuery.Include(x => x.TrainSearchesWagonTypes).ThenInclude(twt => twt.WagonType).ToListAsync();
                return mapper.Map<List<TrainSearch>, List<T>>(source);
            }
            catch (Exception ex)
            {
                return new List<T>();
            }
        }

        public async Task<List<T>> GetUserSearchesAsync<T>(string userId)
            where T : BaseTrainSearchDTO
        {
            try
            {
                var source = await db.TrainSearches.Where(ts => ts.UserId == userId).OrderByDescending(ts=>ts.Date).ToListAsync();
                return mapper.Map<List<TrainSearch>, List<T>>(source);
            }
            catch(Exception ex)
            {
                return new List<T>();
            }
            
        }

        public async Task<int> AddAsync(TrainSearchDTO trainSearchDto)
        {
            int searchId = 0;
            try
            {
                TrainSearch trainSearch = db.TrainSearches.FirstOrDefault(ts => ts.UserId == trainSearchDto.UserId && ts.Date == trainSearchDto.Date && ts.From == trainSearchDto.From && ts.To == trainSearchDto.To);

                if (trainSearch != null)
                    return trainSearch.Id;

                //trainSearch = Mapper.Map<TrainSearch>(trainSearchDto);
                trainSearch = mapper.Map<TrainSearch>(trainSearchDto);
                await db.AddAsync(trainSearch);
                await db.SaveChangesAsync();
                searchId = trainSearch.Id;
            }
            catch { }
            return searchId;
        }

        public async Task<int> AddAsync(AdvancedTrainSearchDTO trainSearchDto)
        {
            int searchId = 0;
            try
            {
                //TrainSearch trainSearch = db.TrainSearches.FirstOrDefault(ts => ts.UserId == trainSearchDto.UserId && ts.Date == trainSearchDto.Date && ts.From == trainSearchDto.From && ts.To == trainSearchDto.To);

                //if (trainSearch != null)
                //    return true;

                TrainSearch trainSearch = mapper.Map<TrainSearch>(trainSearchDto);

                var wagonTypes = trainSearchDto.WagonTypes.Select(x => new TrainSearchesWagonType() { TrainSearchId = trainSearch.Id, WagonTypeId = x.Id }).ToList();
                trainSearch.TrainSearchesWagonTypes = new List<TrainSearchesWagonType>();
                foreach (var wagonType in wagonTypes)
                {
                    trainSearch.TrainSearchesWagonTypes.Add(wagonType);
                }

                await db.AddAsync(trainSearch);
                await db.SaveChangesAsync();



                //await db.SaveChangesAsync();

                searchId = trainSearch.Id;
            }
            catch(Exception ex) {
                searchId = 0;
            }
            return searchId;
        }

        public async Task<bool> UpdateAsync(TrainSearchDTO trainSearchDto)
        {
            bool result = false;

            TrainSearch existingTrainSearch = await db.TrainSearches.FindAsync(trainSearchDto.Id);
            if(existingTrainSearch != null)
            {
                try
                {
                    mapper.Map(trainSearchDto, existingTrainSearch);
                    db.Set<TrainSearch>().Attach(existingTrainSearch);
                    db.Entry(existingTrainSearch).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    result = true;
                }
                catch(Exception ex) { }
            }

            return result;
        }

        public async Task<bool> UpdateAsync(AdvancedTrainSearchDTO advancedTrainSearchDto)
        {
            bool result = false;

            TrainSearch existingTrainSearch = await db.TrainSearches.Include(trainSearch=>trainSearch.TrainSearchesWagonTypes).FirstOrDefaultAsync(trainSearch => trainSearch.Id == advancedTrainSearchDto.Id);
            if (existingTrainSearch != null)
            {
                try
                {
                    mapper.Map(advancedTrainSearchDto, existingTrainSearch);                    

                    var wagonTypes = advancedTrainSearchDto.WagonTypes.Select(x => new TrainSearchesWagonType() { TrainSearchId = advancedTrainSearchDto.Id, WagonTypeId = x.Id }).ToList();
                    existingTrainSearch.TrainSearchesWagonTypes = new List<TrainSearchesWagonType>();                                                                           
                    foreach (var wagonType in wagonTypes)
                    {
                        existingTrainSearch.TrainSearchesWagonTypes.Add(wagonType);
                    }

                    db.Set<TrainSearch>().Attach(existingTrainSearch);                    
                    await db.SaveChangesAsync();
                    result = true;
                }
                catch (Exception ex) { }
            }

            return result;
        }

        public async Task<bool> UpdateSearchStatusAsync(int searchId, bool activeStatus = true)
        {
            bool result = false;

            TrainSearch existingTrainSearch = await db.TrainSearches.FindAsync(searchId);
            if (existingTrainSearch != null)
            {
                try
                {
                    existingTrainSearch.Active = activeStatus;
                    db.Set<TrainSearch>().Attach(existingTrainSearch);
                    db.Entry(existingTrainSearch).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    result = true;
                }
                catch (Exception ex) { }
            }

            return result;
        }

        public bool UpdateSearchStatus(int searchId, bool activeStatus = true)
        {
            bool result = false;

            TrainSearch existingTrainSearch = db.TrainSearches.Find(searchId);
            if (existingTrainSearch != null)
            {
                try
                {
                    existingTrainSearch.Active = activeStatus;
                    db.Set<TrainSearch>().Attach(existingTrainSearch);
                    db.Entry(existingTrainSearch).State = EntityState.Modified;
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception ex) {

                }
            }

            return result;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            bool result = false;
            TrainSearch trainSearch = await db.TrainSearches.FindAsync(id);
            if (trainSearch != null)
            {
                try
                {
                    db.Remove(trainSearch);
                    await db.SaveChangesAsync();
                    result = true;
                }
                catch (Exception ex)
                {

                }
            }
            
            return result;
        }        

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
