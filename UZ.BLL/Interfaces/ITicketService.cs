﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Models;
using UZ.DAL.Models;

namespace UZ.BLL.Interfaces
{
    public interface ITicketService : IDisposable
    {
        DbSet<ReservedTicket> Storage { get; }
        Task<TicketDTO> GetAsync(int id);
        Task<int> AddAsync(TicketDTO ticket);
        int Add(TicketDTO ticket);
        Task<bool> UpdateAsync(TicketDTO ticket);
        bool Update(TicketDTO ticket);
        Task<List<TicketDTO>> GetAllAsync(int? bookingMinutesPeriod = null, bool onlyActive = false);
        Task<bool> UpdateTicketStatusAsync(int ticketId, bool activeStatus = true);
        Task<bool> DeleteAsync(int id);
    }
}
