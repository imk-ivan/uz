﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Models
{
    public class TrainSearchResponse
    {
        public int Error { get; set; }
        public TrainSearchResponseDataList Data { get; set; }
        //public int Captcha { get; set; }
    }

    public class TrainSearchResponseDataList
    {
        public List<TrainSearchData> List { get; set; }
    }

    public class TrainSearchData
    {
        public string Num { get; set; }

        public int Category { get; set; }

        public string TravelTime { get; set; }

        public int AllowStudent { get; set; }

        public int AllowBooking { get; set; }

        public int IsCis { get; set; }

        public bool IsEurope { get; set; }

        public bool AllowPrivilege { get; set; }

        public bool DisabledPrivilegeByDate { get; set; }

        public TrainStation From { get; set; }

        public TrainStation To { get; set; }

        public List<TrainVagon> Types { get; set; }
    }

    public class TrainStation
    {
        public int Code { get; set; }
        public string Station { get; set; }
        public string StationTrain { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string SortTime { get; set; }
        public string SrcDate { get; set; }
    }

    public class TrainVagon
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Letter { get; set; }
        public int Places { get; set; }
    }
       

}
