﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.DAL;
using UZ.DAL.Models;

namespace UZ.BLL.Services
{
    public class BlockedTrainService : IBlockedTrainService
    {
        private ApplicationDbContext db;

        private readonly IMapper mapper;

        public BlockedTrainService(ApplicationDbContext _db, IMapper _mapper)
        {
            db = _db;
            mapper = _mapper;
        }

        public DbSet<BlockedTrain> Storage
        {
            get
            {
                return db.BlockedTrains;
            }
        }

        public async Task<List<BlockedTrainDTO>> GetAllAsync()
        {
            var source = await db.BlockedTrains.ToListAsync();
            return mapper.Map<List<BlockedTrain>, List<BlockedTrainDTO>>(source);
        }

        public async Task<int> AddAsync(BlockedTrainDTO blockedTrain)
        {
            int resultId = 0;
            try
            {
                BlockedTrain existingBlockedTrain = await db.BlockedTrains.FirstOrDefaultAsync(blocked => blocked.TrainId == blockedTrain.TrainId);

                if (existingBlockedTrain != null)
                    return existingBlockedTrain.Id;

                //trainSearch = Mapper.Map<TrainSearch>(trainSearchDto);
                existingBlockedTrain = mapper.Map<BlockedTrain>(blockedTrain);
                await db.AddAsync(existingBlockedTrain);
                await db.SaveChangesAsync();
                resultId = existingBlockedTrain.Id;
            }
            catch { }
            return resultId;
        }

        public async Task<bool> IsBlocked(List<string> trainIds)
        {
            bool result = false;

            result = await db.BlockedTrains.AnyAsync(blockedTrain => trainIds.Contains(blockedTrain.TrainId));

            return result;
        }

        public async Task<bool> RemoveAsync(int id)
        {
            bool result = false;
            BlockedTrain blockedItem = await db.BlockedTrains.FindAsync(id);
            if (blockedItem != null)
            {
                try
                {
                    db.Remove(blockedItem);
                    await db.SaveChangesAsync();
                    result = true;
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
