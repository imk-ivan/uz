﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class TicketStationsStr : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StationFrom",
                table: "ReservedTickets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StationTo",
                table: "ReservedTickets",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StationFrom",
                table: "ReservedTickets");

            migrationBuilder.DropColumn(
                name: "StationTo",
                table: "ReservedTickets");
        }
    }
}
