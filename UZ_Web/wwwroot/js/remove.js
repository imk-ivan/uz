﻿$(function () {
    $(document).on('click', '.remove-item', function (e) {
        e.preventDefault();
        var item = $(this).data('item');
        var confirmMsg = 'Вы уверены что хотите удалить заявку?';
        var actionUrl = '/train/remove-search'

        if (item === 'ticket'){
            confirmMsg = 'Вы уверены что хотите удалить билет?';
            actionUrl = '/admin/ticket/remove'
        }
        else if (item === "blocked") {
            confirmMsg = 'Вы уверены что хотите удалить элемент из ЧС?';
            actionUrl = '/admin/blacklist/remove'
        }


        var currentRow = $(this).parents("tr");
        var currentEl = $(this);
        if (confirm(confirmMsg)) {
            currentEl.parents('.loader').addClass("active");
            currentEl.addClass('hidden');
            $.ajax({
                url: actionUrl,
                data: { id: $(this).data('id') },
                type: 'Delete',
                success: function (data) {
                    $("#Notifications").html(data).addClass("show");                    
                    if (data.indexOf("alert-success") !== -1) {
                        currentRow.addClass("hidden");
                    }                    
                },
                complete: function () {
                    currentEl.parents('.loader').removeClass("active");
                    currentEl.removeClass('hidden');
                }
            });
        }
    })
})
