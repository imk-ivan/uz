﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UZ.DAL.Models
{
    public class WagonType
    {
        [Key]
        public string Id { get; set; }
        public string Title { get; set; }

        public ICollection<TrainSearchesWagonType> TrainSearchesWagonTypes { get; set; }
    }
}
