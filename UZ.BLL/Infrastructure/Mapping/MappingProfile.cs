﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UZ.BLL.Models;
using UZ.DAL.Models;

namespace UZ.BLL.Infrastructure.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TrainSearch, AdvancedTrainSearchDTO>()
                .ForMember(x => x.TrainsIds, opt => opt.MapFrom(z => z.TrainsIds.Split(',').ToList()))
                .ForMember(x => x.WagonTypes, opt => opt.MapFrom(z => z.TrainSearchesWagonTypes.Select(x=>x.WagonType).ToList()));
            CreateMap<AdvancedTrainSearchDTO, TrainSearch>()
                .ForMember(x => x.TrainsIds, opt => opt.MapFrom(z => String.Join(",", z.TrainsIds)))
                .ForMember(x => x.TrainSearchesWagonTypes, opt => opt.Ignore());

            CreateMap<TrainSearch, TrainSearchDTO>().ReverseMap();

            CreateMap<WagonType, WagonTypeDto>().ReverseMap();

            CreateMap<ReservedTicket, TicketDTO>().ReverseMap();

            CreateMap<BlockedTrain, BlockedTrainDTO>().ReverseMap();
        }   
    }
}
