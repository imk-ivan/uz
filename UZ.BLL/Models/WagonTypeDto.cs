﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Models
{
    public class WagonTypeDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}
