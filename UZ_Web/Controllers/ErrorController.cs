﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace UZ.Web.Controllers
{
    //[Route("error")]
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ErrorStatus(string id)
        {
            return View("~/Views/Error/Error404.cshtml",$"Статуcный код ошибки: {id}");
        }
    }
}