﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.Enums
{
    public enum SortState
    {
        DateAsc,
        DateDesc,
        FromAsc,
        FromDesc,
        ToAsc,
        ToDesc,
        FirstNameAsc,
        FirstNameDesc,
        LastNameAsc,
        LastNameDesc,
        EmailAsc,
        EmailDesc
    }
}
