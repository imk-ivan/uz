﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Models;

namespace UZ.BLL.Interfaces
{
    public interface IWagonTypeService
    {
        Task<bool> AddAsync(WagonTypeDto wagonType);
        Task<List<WagonTypeDto>> GetAllAsync();
        List<WagonTypeDto> GetAll();
    }
}
