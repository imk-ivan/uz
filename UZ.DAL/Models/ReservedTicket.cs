﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace UZ.DAL.Models
{
    public class ReservedTicket
    {
        [Key]
        public int Id { get; set; }

        public int Ord { get; set; }

        public int From { get; set; }

        public string StationFrom { get; set; }

        public int To { get; set; }

        public string StationTo { get; set; }

        public string Train { get; set; }

        public DateTime Date { get; set; }

        public DateTime ArrivalDate { get; set; }

        public string TravelTime { get; set; }

        public int WagonNumber { get; set; }

        public string WagonClass { get; set; }

        public string WagonType { get; set; }

        public int WagonRailway { get; set; }

        public string Charline { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Bedding { get; set; }

        //public string[] Services { get; set; }

        public string Child { get; set; }

        public string Student { get; set; }

        public int Reserve { get; set; }

        public int PlaceNumber { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string SessionId { get; set; }

        public DateTime ReservedTime {get; set;}

        public bool Active { get; set; }

        public string ReserveId { get; set; }

        public int? TrainSearchId { get; set; }

        [ForeignKey(nameof(TrainSearchId))]
        public virtual TrainSearch TrainSearch { get; set; }

        public int RebookedCounter { get; set; }
    }
}
