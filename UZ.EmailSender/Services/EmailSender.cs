﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using UZ.EmailSender.Interfaces;

namespace UZ.EmailSender.Services
{
    public class EmailSender : IEmailSender
    {
        public IEmailConfiguration _config;

        public EmailSender(IEmailConfiguration config)
        {
            _config = config;
        }

        public async Task SendEmailAsync(string email, string subject, string message, bool isBodyHtml = false)
        {
            SmtpClient client = GetClient();
            MailMessage mailMessage = GetMailMessage(email, subject, message, isBodyHtml);

            await client.SendMailAsync(mailMessage);
        }

        public void SendEmail(string email, string subject, string message, bool isBodyHtml = false)
        {
            SmtpClient client = GetClient();
            MailMessage mailMessage = GetMailMessage(email, subject, message, isBodyHtml);

            client.Send(mailMessage);
        }

        private SmtpClient GetClient()
        {
            SmtpClient client = new SmtpClient(_config.SmtpServer,_config.Port);
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(_config.Login, _config.Password);
            client.EnableSsl = true;
            return client;
        }

        private MailMessage GetMailMessage(string email, string subject, string message, bool isBodyHtml = false)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("UZ@Web");
            mailMessage.To.Add(email);
            mailMessage.Body = message;
            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = isBodyHtml;
            return mailMessage;
        }

    }
}
