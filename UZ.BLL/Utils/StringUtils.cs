﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace UZ.BLL.Utils
{
    public static class StringUtils
    {
        public static List<string> GetReserveIdFromCartHtml(string cartHtml)
        {
            List<string> reserveIds = new List<string>();

            //Regex regex = new Regex(string.Format(@"<[^>]*\b{0}\s*=\s*(['""])(.*)\1[^>]*>", "data-reserve-id"));
            //Regex regex = new Regex(@"(?<=\data-reserve-id="")[^""]*");
            //if (regex.IsMatch(cartHtml)) {
            //    MatchCollection matches = regex.Matches(cartHtml);
            //    foreach(Match match in matches)
            //    {
            //        reserveIds.Add(match.Groups[0].Value);
            //    }
            //}

            HtmlDocument cartDoc = new HtmlDocument();
            cartDoc.LoadHtml(cartHtml);
            HtmlNodeCollection nodes = cartDoc.DocumentNode.SelectNodes("//div[contains(@class, 'b-cart--passenger')]");

            if(nodes != null)
            {
                foreach (var node in nodes)
                {
                    string reserveId = node.GetAttributeValue("data-reserve-id", null);
                    if (!String.IsNullOrEmpty(reserveId))
                    {
                        reserveIds.Add(reserveId);
                    }
                }
            }
            
            return reserveIds;
        }
    }
}
