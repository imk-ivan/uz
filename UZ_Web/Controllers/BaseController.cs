﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UZ.BLL.Infrastructure.UZRequestModule;

namespace UZ.Web.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IUZRequestService UZRequestService;

        public BaseController(IUZRequestService _uzService)
        {
            UZRequestService = _uzService;
        }
    }
}