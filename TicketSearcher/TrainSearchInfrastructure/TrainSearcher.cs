﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Infrastructure.Proxy;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Models;

namespace TicketSearcher.TrainSearchInfrastructure
{
    public class TrainSearcher
    {
        public static IUZRequestService uZRequestService;
        private ProxyConfig proxyConfig; 

        public TrainSearcher(IUZRequestService _UZRequestService, ProxyConfig _proxyConfig)
        {
            uZRequestService = _UZRequestService;
            proxyConfig = _proxyConfig;
        }

        public List<WagonPlace> Search(AdvancedTrainSearchDTO searchFilter)
        {
            List<WagonPlace> places = null;
            try
            {
                foreach (var trainId in searchFilter.TrainsIds)
                {
                    places = CheckTrain(trainId, searchFilter);
                    if (places != null)
                        break;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Error with search, ID - {searchFilter.Id}. Erroe message:{ex.Message}");
            }
            //if (searchFilter.WagonTypes.Count == 0)
            //    return places;

            
            return places;
        }

        public List<WagonPlace> CheckTrain(string trainId, AdvancedTrainSearchDTO searchFilter)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            List<WagonPlace> places = null;

            //Get all train wagon types
            var trainGeneralData = uZRequestService.GetTrainWagons(searchFilter.From, searchFilter.To, searchFilter.Date.ToString("yyyy-MM-dd"), trainId, null);

            if (trainGeneralData == null || trainGeneralData.Error > 0 || trainGeneralData.Data == null)
                return places;

            //Get wagon types ids
            List<string> wagonTypes = searchFilter.WagonTypes.Select(x => x.Id).ToList();

            var expectedTypes = trainGeneralData.Data.Types.Where(x => wagonTypes.Contains(x.Type_Id) && x.Free > 0).ToList();

            foreach (var type in expectedTypes)
            {
                places = CheckTrainWagons(trainId, type.Type_Id, searchFilter);
                if (places != null)
                    break;
            }

            stopwatch.Stop();
            Console.WriteLine($"Время выполнения поиска - {stopwatch.Elapsed}, мест - {places?.Count ?? 0}");
            return places;
        }

        public List<WagonPlace> CheckTrainWagons(string trainId, string wagonTypeId, AdvancedTrainSearchDTO searchFilter)
        {
            List<WagonPlace> places = null;
            uZRequestService.InitializeProxy(proxyConfig.Credentials, proxyConfig.GetAddress());
            var trainData = uZRequestService.GetTrainWagons(searchFilter.From, searchFilter.To, searchFilter.Date.ToString("yyyy-MM-dd"), trainId, wagonTypeId);

            //If free places existing
            foreach(var wagon in trainData.Data.Wagons.Where(x => x.Free > 0))
            {
                places = CheckWagon(trainId, wagon.Num, wagon.Type, wagon.Class, searchFilter);
                if (places != null)
                {
                    foreach(WagonPlace place in places)
                        place.Railway = wagon.Railway;

                    break;
                }
                    
            }
            return places;
        }

        public List<WagonPlace> CheckWagon(string trainId, int wagonNum, string wagonType, string wagonClass, AdvancedTrainSearchDTO searchFilter)
        {
            List<WagonPlace> places = null;
            uZRequestService.InitializeProxy(proxyConfig.Credentials, proxyConfig.GetAddress());
            WagonPlacesResponse wagonPlaces = uZRequestService.GetWagonPlaces(searchFilter.From, searchFilter.To, searchFilter.Date.ToString("yyyy-MM-dd"), trainId, wagonType, wagonNum, wagonClass);

            try
            {
                if (wagonPlaces.Data.Places.Count > 0)
                {
                    //Stopwatch stopwatch = new Stopwatch();
                    //stopwatch.Start();
                    foreach (KeyValuePair<string, List<int>> entry in wagonPlaces.Data.Places)
                    {
                        List<int> placesList = entry.Value;
                        places = placesList.Select(x => new WagonPlace() { WagonTypeId = wagonType, WagonNumber = wagonNum, PlaceNumber = x, TrainId = trainId, WagonClass = wagonClass }).ToList();

                        if (searchFilter.BottomOnly && wagonType != "С" && wagonType != "Л")
                        {
                            places = places.Where(x => x.PlaceNumber % 2 != 0).ToList();
                        }

                        if (places.Count > 0)
                        {
                            break;
                        }


                    }
                    //stopwatch.Stop();
                    //Console.WriteLine($"Обработка мест: {stopwatch.Elapsed}");
                }
                
            }
            catch(Exception ex) { }

            return places;
        }

    }
}
