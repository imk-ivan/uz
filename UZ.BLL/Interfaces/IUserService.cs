﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UZ.BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<List<string>> GetAdminEmails();
    }
}
