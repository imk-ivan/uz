﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.EmailSender.Interfaces
{
    public interface IEmailConfiguration
    {
        string SmtpServer { get; }
        int Port { get; }
        string Login { get; }
        string Password { get; }
    }
}
