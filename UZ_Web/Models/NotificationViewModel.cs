using System;

namespace UZ.Web.Models
{
    public class NotificationViewModel
    {
        public bool Success { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }  
        
        public NotificationViewModel(bool success, string title, string description)
        {
            Success = success;
            Title = title;
            Description = description;
        }

        public NotificationViewModel(bool success, string successTitle, string successDescription, string errorTitle, string errorDescription)
        {
            Success = success;
            if (success)
            {
                Title = successTitle;
                Description = successDescription;
            }
            else
            {
                Title = errorTitle;
                Description = errorDescription;
            }
            
        }

    }
}