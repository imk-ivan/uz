﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Models
{
    public class OrderDto
    {
        [JsonProperty("roundTrip")]
        public int RoundTrip { get; set; }
        [JsonProperty("places")]
        public List<TicketDTO> Places { get; set; }

        public OrderDto(List<TicketDTO> _places)
        {
            RoundTrip = 0;
            Places = _places;
        }
    }
}
