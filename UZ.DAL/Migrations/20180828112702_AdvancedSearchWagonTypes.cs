﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class AdvancedSearchWagonTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WagonType",
                table: "TrainSearches",
                newName: "WagonTypes");

            migrationBuilder.AddColumn<string>(
                name: "TrainsIds",
                table: "TrainSearches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TrainsIds",
                table: "TrainSearches");

            migrationBuilder.RenameColumn(
                name: "WagonTypes",
                table: "TrainSearches",
                newName: "WagonType");
        }
    }
}
