﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UZ.BLL.Infrastructure.Encryption;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.DAL.Models;
using UZ.Web.Models.SearchViewModels;
using UZ.Web.Models;
using UZ.EmailSender.Services;
using Microsoft.Extensions.Configuration;
using UZ.EmailSender.Interfaces;
using System.IO;

namespace UZ.Web.Controllers
{
    [Route("train")]
    public class TrainController : BaseController
    {
        protected IDBService dBService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly List<WagonTypeDto> WagonTypes;
        private readonly IEmailSender _emailSender;



        public TrainController(IDBService _dBService, IUZRequestService _uzService, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IEmailSender emailSender)
            : base(_uzService)
        {
            dBService = _dBService;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            WagonTypes = dBService.WagonTypeService.GetAll();
            
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("search")]
        public async Task<IActionResult> Search(TrainSearchDTO trainSearch)
        {
            if (!ModelState.IsValid)
            {
                return View(trainSearch);
            }
            if (User.Identity.IsAuthenticated)
            {
                trainSearch.UserId = _userManager.GetUserId(User);
            }

            List<TrainSearchData> trains = UZRequestService.TrainSearch(trainSearch);            
            SearchResultViewModel result = new SearchResultViewModel()
            {
                Search = new AdvancedTrainSearchDTO() {
                    From = trainSearch.From,
                    To = trainSearch.To,
                    StationFrom = trainSearch.StationFrom,
                    StationTo = trainSearch.StationTo,
                    Date = trainSearch.Date
                },
                Trains = trains
            };

            ViewBag.WagonTypes = WagonTypes;

            return View("SearchResult", result);
        }

        [HttpPost("save-search")]
        public async Task<IActionResult> SaveSearch(AdvancedTrainSearchDTO Search)
        {
            bool result = false;

            NotificationViewModel notificationModel = new NotificationViewModel(result, "Отлично!", "Как только появятся свободные места, мы вам сообщим.",
                "Упс!", "Не удалось добавить заявку на поиск. При сохранении возникли ошибки.");

            if (ModelState.IsValid)
            {
                bool isBlocked = await dBService.BlackListService.IsBlocked(Search.TrainsIds);
                if (isBlocked)
                {
                    notificationModel.Description = "Один или несколько из выбранных поездов находятся в черном списке.";
                }
                else
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        Search.UserId = _userManager.GetUserId(User);
                    }
                    Search.Active = true;
                    var searchId = await dBService.TrainSearchService.AddAsync(Search);
                    if (searchId !=0)
                        result = true;

                    notificationModel.Success = result;
                    if (result) {
                        notificationModel.Title = "Отлично!";
                        notificationModel.Description = "Мы сообщим Вам как только появятся билеты соответствующие Вашим критериям поиска.";

                       
                        _emailSender.SendEmail(Search.Email, "Билеты на поезд",
                            $"Ваша заявка на поиск билетов на {Search.Date.ToString("dd.MM.yyyy")} {Search.StationFrom} - {Search.StationTo} успешно принята. Мы сообщим Вам как только появятся билеты соответствующие Вашим критериям поиска. <br> <a href='{Url.Action("UnsubscribeTrainSearch", "Train", new { searchIdStr = Encrypt.EncryptString(searchId.ToString(), "unsubscribetrainsearch")  }, Request.Scheme)}' target='_blank'>Отписаться от рассылки</a>", true);
                    }
                }
       
            }                       

            return PartialView("_Notification", notificationModel);            
        }

        [Authorize(Roles = "admin")]
        [HttpPost("update-search")]
        public async Task<IActionResult> UpdateSearch(AdvancedTrainSearchDTO Search)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                 result = await dBService.TrainSearchService.UpdateAsync(Search); 
            }

            NotificationViewModel notificationModel = new NotificationViewModel(result, "Отлично!", "Заявка успешно сохранена.",
                "Упс!", "Не удалось добавить заявку на поиск. При сохранении возникли ошибки.");

            return PartialView("_Notification", notificationModel);
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("remove-search")]
        public async Task<IActionResult> RemoveSearch(int id)
        {
            bool result = false;
           
            result = await dBService.TrainSearchService.DeleteAsync(id);

            NotificationViewModel notificationModel = new NotificationViewModel(result, "Отлично!", "Заявка успешно удалена.", 
                "Упс!", "При удалении возникли ошибки.");

            return PartialView("_Notification", notificationModel);
        }

        [Authorize]
        [HttpGet("my-searches")]
        public async Task<IActionResult> UserSearches()
        {
            string userId = _userManager.GetUserId(User);
            List<TrainSearchDTO> searches = await dBService.TrainSearchService.GetUserSearchesAsync<TrainSearchDTO>(userId);
            return View(searches);
        }

        [Authorize]
        [HttpPost("set-search-status")]
        public async Task<IActionResult> SetSearchStatus(int searchId, bool status)
        {
            bool success = false;
            TrainSearchDTO search = await dBService.TrainSearchService.GetAsync<TrainSearchDTO>(searchId);
            if(search != null && search.UserId == _userManager.GetUserId(User))
            {
                search.Active = status;
                success = await dBService.TrainSearchService.UpdateAsync(search);
            }

            return Ok(success);
        }

        [HttpGet("unsubscribe-train-search/{searchIdStr}")]
        public async Task<IActionResult> UnsubscribeTrainSearch(string searchIdStr)
        {
            bool success = false;
            bool parseResult = int.TryParse(Encrypt.DecryptString(searchIdStr, "unsubscribetrainsearch"), out int searchId);            
            if (parseResult)
            {
                TrainSearchDTO search = await dBService.TrainSearchService.GetAsync<TrainSearchDTO>(searchId);
                if (search != null)
                {
                    search.Active = false;
                    success = await dBService.TrainSearchService.UpdateAsync(search);
                }
            }

            NotificationViewModel notificationModel = new NotificationViewModel(success, "Готово.", "Заявка успешно удалена.",
                "Упс!", "При отмене заявки на поиск возникли ошибки.");

            return View(notificationModel);
        }

    }
}