﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Models
{
    public class TrainWagonsResponse
    {
        public int Error { get; set; }
        public TrainWagonResponseData Data { get; set; }
    }
}
