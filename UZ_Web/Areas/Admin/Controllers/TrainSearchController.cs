﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.Enums;
using UZ.Web.Areas.Models;
using UZ.Web.Controllers;

namespace UZ.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin")]
    [Authorize(Roles = "admin")]
    public class TrainSearchController : BaseController
    {
        protected IDBService _dBService;

        private readonly List<WagonTypeDto> WagonTypes;

        public TrainSearchController(IUZRequestService _UZRequestService, IDBService dBService)
            :base(_UZRequestService)
        {
            _dBService = dBService;
            WagonTypes = _dBService.WagonTypeService.GetAll();
        }

        public IActionResult Index()
        {
            var model = _dBService.TrainSearchService.Storage.OrderBy(ts => ts.Id);

            return View(model);
        }

        [HttpGet("edit/{id}")]
        public async Task<IActionResult> Edit(int id) {
            AdvancedTrainSearchDTO search = await _dBService.TrainSearchService.GetAsync<AdvancedTrainSearchDTO>(id);
            ViewBag.WagonTypes = WagonTypes;
            return View(search);
        }
    }
}
