﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Models
{
    public class TicketResponse
    {
        public int Error { get; set; }
        public TicketResponseData Data { get; set; }
        public int Captcha { get; set; }
    }

    public class TicketResponseData
    {
        public int[] IgnoredPlaces { get; set; }
        public string[] Error { get; set; }
        public int PlacesCount { get; set; }
        public int CartCount { get; set; }
        //public string Analytics { get; set; }
    }
}
