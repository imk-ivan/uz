﻿using System.Collections.Generic;

namespace UZ.BLL.Models
{
    public class TrainWagonResponseData
    {
        public List<TrainWagonType> Types { get; set; }
        public List<TrainWagon> Wagons { get; set; }
    }

    public class TrainWagonType
    {
        public string Type_Id { get; set; }
        public string Title { get; set; }
        public string Letter { get; set; }
        public int Free { get; set; }
        public int Cost { get; set; }
        public bool IsOneCost { get; set; }
    }

    public class TrainWagon
    {
        public int Num { get; set; }
        public string Type_Id { get; set; }
        public string Type { get; set; }
        public string Class { get; set; }
        public int Railway { get; set; }
        public int Free { get; set; }
        public bool IsOneCost { get; set; }
        public bool ByWishes { get; set; }
        public bool HasBedding { get; set; }
        public string[] Services { get; set; }
        public Dictionary<string, int> Prices { get; set; }
        public int ReservePrice { get; set; }
        public bool AllowBonus { get; set; }
        public string Air { get; set; }

    }
}