﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Models;
using UZ.DAL.Models;

namespace UZ.BLL.Interfaces
{
    public interface IBlockedTrainService : IDisposable
    {
        DbSet<BlockedTrain> Storage { get; }
        Task<List<BlockedTrainDTO>> GetAllAsync();
        Task<int> AddAsync(BlockedTrainDTO blockedTrain);
        Task<bool> RemoveAsync(int id);
        Task<bool> IsBlocked(List<string> trainIds);
    }
}
