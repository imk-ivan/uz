﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using TicketSearcher.TrainSearchInfrastructure;
using UZ.BLL.Infrastructure.Mapping;
using UZ.BLL.Infrastructure.Proxy;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Infrastructure.UZRequestModule.Configuration;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.BLL.Services;
using UZ.BLL.Utils;
using UZ.DAL;
using UZ.EmailSender.Interfaces;
using UZ.EmailSender.Services;
using UZ.Enums;
using Microsoft.AspNetCore.Mvc;
using UZ.BLL.Extensions;
using TicketSearcher.LinkGeneration;

namespace TicketSearcher
{
    class Program
    {
        public static IConfiguration Configuration;
        public static IDBService db;
        public static IMapper mapper;
        public static IUZRequestConfiguration requestConfig;
        public static IUZRequestService UZRequestService;
        public static IEmailConfiguration EmailConfig;
        public static readonly string API_BASE;
        public const int TIME_PERIOD = 3000;

        public static ProxyConfig proxyConfig;

        public const int ThreadCount = 10;

        public static List<string> AdminEmails;

        static Program()
        {
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (String.IsNullOrWhiteSpace(environment))
                environment = "Production";
                //throw new ArgumentNullException("Environment not found in ASPNETCORE_ENVIRONMENT");

            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("appsettings.json", optional: true);
            if (environment == "Development")
            {

                builder
                    .AddJsonFile(
                        Path.Combine(AppContext.BaseDirectory, string.Format("..{0}..{0}..{0}", Path.DirectorySeparatorChar), $"appsettings.{environment}.json"),
                        optional: true
                    );
            }
            //else
            //{
            //    builder
            //        .AddJsonFile($"appsettings.{environment}.json", optional: false);
            //}

            Configuration = builder.Build();

            // Add application services.
            var serviceProvider = new ServiceCollection()
                .AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")))
                .AddScoped(typeof(IDBService), typeof(DBService))
                .AddAutoMapper(x => x.AddProfile(new MappingProfile()))
                .AddSingleton<IUZRequestConfiguration>(Configuration.GetSection("UZRequestConfiguration").Get<UZRequestConfiguration>())
                .AddTransient<IUZRequestService, UZRequestService>()
                .BuildServiceProvider();

            API_BASE = Configuration.GetSection("ApiUrl").Get<String>();

            mapper = serviceProvider.GetService<IMapper>();
            EmailConfig = Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>();
            //db = serviceProvider.GetService<IDBService>();
            db = GetDBService();
            
            UZRequestService = serviceProvider.GetService<IUZRequestService>();

            requestConfig = Configuration.GetSection("UZRequestConfiguration").Get<UZRequestConfiguration>();

            string proxiesPath = Path.Combine(AppContext.BaseDirectory, "Proxy.txt");
            List<string> proxies = GetFileList(proxiesPath);

            //proxyConfig = new ProxyConfig("UA275858", "R66LBNhbIR", proxies, ProxyUsingType.Consistently);          
            proxyConfig = new ProxyConfig();          
        }

        public static async Task Main(string[] args)
        {
            while (true)
            {
                //var z = Search();
                //z.Start();

                AdminEmails = await db.UserService.GetAdminEmails();
                List<AdvancedTrainSearchDTO> searches = await GetAllSearches();
                int searchesCount = searches.Count;
                //Stopwatch sw = new Stopwatch();
                //sw.Start();
                SearchMultiThreading(searches);
                //for (int i = 0; i<= searchesCount; i += ThreadCount)
                //{

                //    var currentSearches = searches.Skip(i).Take(ThreadCount).ToList();

                //    SearchMultiThreading(currentSearches);
                //}
                //sw.Stop();
                //Console.WriteLine(sw.Elapsed);
            }
        }

        private static void SearchMultiThreading(List<AdvancedTrainSearchDTO> searches)
        {
            Parallel.ForEach(
                    searches,
                    new ParallelOptions { MaxDegreeOfParallelism = ThreadCount },
                    search => { WriteSearch(search); }
                );
            //foreach (var search in searches)
            //{
            //    WriteSearch(search);
            //}
            //Thread.Sleep(TIME_PERIOD);
        }

        public static void WriteSearch(AdvancedTrainSearchDTO trainSearch)
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            IUZRequestService uzService = new UZRequestService(requestConfig, proxyConfig.Credentials, proxyConfig.GetAddress());

            TrainSearcher searcher = new TrainSearcher(uzService, proxyConfig);

            List<WagonPlace> places = searcher.Search(trainSearch);

            if (places != null)
            {
                foreach(var place in places)
                {
                    TicketDTO ticket = new TicketDTO()
                    {
                        Ord = 0,
                        From = trainSearch.From,
                        StationFrom = trainSearch.StationFrom,
                        To = trainSearch.To,
                        StationTo = trainSearch.StationTo,
                        Train = place.TrainId,
                        Date = trainSearch.Date,
                        WagonNumber = place.WagonNumber,
                        WagonClass = place.WagonClass,
                        WagonType = place.WagonTypeId,
                        WagonRailway = 40,
                        //Charline = "А",
                        FirstName = trainSearch.FirstName,
                        LastName = trainSearch.LastName,
                        Bedding = trainSearch.Bedding ? 1 : 0,
                        //Services = new string[] { "Н", "М" },
                        Child = "",
                        Student = "",
                        Reserve = 0,
                        PlaceNumber = place.PlaceNumber,
                        Email = trainSearch.Email,
                        Phone = trainSearch.Phone,
                        Active = true,
                        TrainSearchId = trainSearch.Id,
                        RebookedCounter = 1
                    };

                    TrainSearchData wagonTrain = GetWagonTrain(uzService, trainSearch, place.TrainId);

                    if(wagonTrain != null)
                    {
                        ticket.TravelTime = wagonTrain.TravelTime;
                        ticket.SetDates(wagonTrain.From.Date, wagonTrain.From.Time, wagonTrain.To.Date, wagonTrain.To.Time);
                    }

                    OrderDto order = new OrderDto(new List<TicketDTO>() { ticket });

                    UZRequestService.InitializeProxy(proxyConfig.Credentials, proxyConfig.GetAddress());

                    TicketResponse result = UZRequestService.AddTicketToCart(order, out string sessionId);
                    if (result != null && result.Error == 0)
                    {
                        IDBService newDbService = GetDBService();

                        ticket.SessionId = sessionId;
                        ticket.ReservedTime = DateTime.Now;
                        Console.WriteLine(sessionId);

                        int newTicketId = newDbService.TicketService.Add(ticket);
                        newDbService.TrainSearchService.UpdateSearchStatus(trainSearch.Id, false);
                        newDbService.Dispose();

                        IEmailSender emailSender = new EmailSender(EmailConfig);

                        string ticketLink = LinkGenerator.GenerateAdminTicketLink(API_BASE, newTicketId);
                        foreach(string email in AdminEmails)
                        {
                            emailSender.SendEmail(email, "Билеты на поезд",
                             $"Новый <a href='{ticketLink}'>билет</a> на поезд добавлен в корзину.",true);
                        }                       

                        break;
                    }

                }
                
            }

            //stopwatch.Stop();
            //Console.WriteLine($"SearchMethodTime: {stopwatch.Elapsed}, places:{places?.Count ?? 0}");
        }

        public static async Task<List<AdvancedTrainSearchDTO>> GetAllSearches()
        {
            List<AdvancedTrainSearchDTO> searches = new List<AdvancedTrainSearchDTO>();
            searches = await db.TrainSearchService.GetAllSearchesAsync<AdvancedTrainSearchDTO>(DateTime.Now.Date, true);
            return searches;
        }

        public static List<string> GetFileList(string filePath)
        {
            List<string> fileList = new List<string>();
            using (StreamReader sr = new StreamReader(filePath, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    fileList.Add(line);
                }
            }
            return fileList;
        }

        public static TrainSearchData GetWagonTrain(IUZRequestService uzService, AdvancedTrainSearchDTO trainSearch, string trainId)
        {
            List<TrainSearchData> trains = uzService.TrainSearch(trainSearch);
            return trains.FirstOrDefault(train => train.Num == trainId);
        }

        private static IDBService GetDBService()
        {
            IDBService newDb;
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            ApplicationDbContext newContext = new ApplicationDbContext(optionsBuilder.Options);
            newDb = new DBService(newContext, mapper);

            return newDb;
        }

    }
}
