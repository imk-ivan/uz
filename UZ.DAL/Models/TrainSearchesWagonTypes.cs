﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.DAL.Models
{
    public class TrainSearchesWagonType
    {
        public int TrainSearchId { get; set; }
        public TrainSearch TrainSearch { get; set; }

        public string WagonTypeId { get; set; }
        public WagonType WagonType { get; set; }
    }
}
