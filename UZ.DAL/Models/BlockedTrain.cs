﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UZ.DAL.Models
{
    public class BlockedTrain
    {
        [Key]
        public int Id { get; set; }

        public string TrainId { get; set; }

        public DateTime BlockedDate { get; set; }
    }
}
