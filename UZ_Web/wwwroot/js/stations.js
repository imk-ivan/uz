﻿$.validator.setDefaults({ ignore: null });
$(function () {
    $(document).ready(function () {
        $("#StationFrom, #StationTo").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/stationssearch',
                    type: 'GET',
                    cache: false,
                    data: request,
                    dataType: 'json',
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.title,
                                value: item.value + ""
                            }
                        }))
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                var inputId = $(this).attr('id');
                if (inputId == "StationFrom") {
                    $('#StationFrom').val(ui.item.label);
                    $('#From').val(ui.item.value).valid();
                }
                else {
                    $('#StationTo').val(ui.item.label);
                    $('#To').val(ui.item.value).valid();
                }

                return false;
            },
            //change: function (event, ui) {
            //    var inputId = $(this).attr('id');
            //    if (inputId == "StationFrom") {
            //        $('#From').val('');
            //    }
            //    else {
            //        $('#To').val('');
            //    }
            //}
        });
    });



})