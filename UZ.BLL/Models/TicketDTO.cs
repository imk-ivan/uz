﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace UZ.BLL.Models
{
    public class TicketDTO
    {
        public int Id { get; set; }

        [JsonProperty("ord")]
        public int Ord { get; set; }
        
        [JsonProperty("from")]
        public int From { get; set; }

        [DisplayName("Откуда")]
        public string StationFrom { get; set; }
                
        [JsonProperty("to")]
        public int To { get; set; }

        [DisplayName("Куда")]
        public string StationTo { get; set; }

        [DisplayName("Поезд")]
        [JsonProperty("train")]        
        public string Train { get; set; }

        [DisplayName("Дата отправления")]
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [DisplayName("Дата прибытия")]
        public DateTime ArrivalDate { get; set; }

        [DisplayName("Время в пути")]
        public string TravelTime { get; set; }

        [DisplayName("Номер вагона")]
        [JsonProperty("wagon_num")]
        public int WagonNumber { get; set; }

        [DisplayName("Класс вагона")]
        [JsonProperty("wagon_class")]
        public string WagonClass { get; set; }

        [DisplayName("Тип вагона")]
        [JsonProperty("wagon_type")]
        public string WagonType { get; set; }
        
        [JsonProperty("wagon_railway")]
        public int WagonRailway { get; set; }

        [JsonProperty("charline")]
        public string Charline { get; set; }

        [DisplayName("Имя")]
        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [DisplayName("Фамилия")]
        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [DisplayName("С бельем")]
        [JsonProperty("bedding")]
        public int Bedding { get; set; }

        [JsonProperty("services")]
        public string[] Services { get; set; }
                
        [JsonProperty("child")]
        public string Child { get; set; }
        
        [JsonProperty("student")]
        public string Student { get; set; }

        [JsonProperty("reserve")]
        public int Reserve { get; set; }

        [DisplayName("Номер места")]
        [JsonProperty("place_num")]
        public int PlaceNumber { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Телефон")]
        public string Phone { get; set; }

        [DisplayName("Id сессии")]
        public string SessionId { get; set; }

        public DateTime ReservedTime { get; set; }

        [DisplayName("Активен")]
        public bool Active { get; set; }

        public string ReserveId { get; set; }

        public int? TrainSearchId { get; set; }

        public int RebookedCounter { get; set; }
    }
}
