﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class TrainSearchesWagonTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WagonTypes",
                table: "TrainSearches");

            migrationBuilder.CreateTable(
                name: "WagonTypes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WagonTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TrainSearchesWagonType",
                columns: table => new
                {
                    TrainSearchId = table.Column<int>(nullable: false),
                    WagonTypeId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainSearchesWagonType", x => new { x.TrainSearchId, x.WagonTypeId });
                    table.ForeignKey(
                        name: "FK_TrainSearchesWagonType_TrainSearches_TrainSearchId",
                        column: x => x.TrainSearchId,
                        principalTable: "TrainSearches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrainSearchesWagonType_WagonTypes_WagonTypeId",
                        column: x => x.WagonTypeId,
                        principalTable: "WagonTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TrainSearchesWagonType_WagonTypeId",
                table: "TrainSearchesWagonType",
                column: "WagonTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TrainSearchesWagonType");

            migrationBuilder.DropTable(
                name: "WagonTypes");

            migrationBuilder.AddColumn<string>(
                name: "WagonTypes",
                table: "TrainSearches",
                nullable: true);
        }
    }
}
