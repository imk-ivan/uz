﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UZ.Web.Areas.Models
{
    public enum AdminMenuActiveType
    {
        Searches,
        Tickets,
        BlackList
    }
}
