﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using UZ.BLL.Interfaces;
using UZ.DAL;

namespace UZ.BLL.Services
{
    public class DBService : IDBService
    {
        private ApplicationDbContext db;

        private readonly IMapper mapper;

        public DBService(ApplicationDbContext _db, IMapper _mapper)
        {
            db = _db;
            mapper = _mapper;
        }

        private ITrainSearchService _trainSearch;

        public ITrainSearchService TrainSearchService
        {
            get
            {
                if(_trainSearch == null)
                {
                    _trainSearch = new TrainSearchService(db, mapper);
                }
                return _trainSearch;
            }
        }

        private IWagonTypeService _wagonTypeService;

        public IWagonTypeService WagonTypeService
        {
            get
            {
                if (_wagonTypeService == null)
                {
                    _wagonTypeService = new WagonTypeService(db, mapper);
                }
                return _wagonTypeService;
            }
        }

        private ITicketService _ticketService;

        public ITicketService TicketService {
            get
            {
                if (_ticketService == null)
                {
                    _ticketService = new TicketService(db, mapper);
                }
                return _ticketService;
            }
        }

        private IBlockedTrainService _blackListService;

        public IBlockedTrainService BlackListService
        {
            get
            {
                if(_blackListService == null)
                {
                    _blackListService = new BlockedTrainService(db, mapper);
                }
                return _blackListService;
            }
        }

        private IUserService _userService;

        public IUserService UserService
        {
            get
            {
                if(_userService == null)
                {
                    _userService = new UserService(db, mapper);
                }
                return _userService;
            }
        }

        public void Dispose()
        {
            if (_trainSearch != null) _trainSearch.Dispose();
            if (_ticketService != null) _ticketService.Dispose();
            if (_blackListService != null) _blackListService.Dispose();
            if (_userService != null) _userService.Dispose();
            db.Dispose();
        }
    }
}
