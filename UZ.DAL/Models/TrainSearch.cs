﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using UZ.Enums;

namespace UZ.DAL.Models
{
    [Table("TrainSearches")]
    public class TrainSearch
    {
        [Key]
        public int Id { get; set; }

        public int From { get; set; }

        public string StationFrom { get; set; }

        public int To { get; set; }

        public string StationTo { get; set; }

        public DateTime Date { get; set; }

        public string UserId { get; set; }

        //[ForeignKey(nameof(UserId))]
        //public virtual ApplicationUser User { get; set; }

        public bool Active { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public bool BottomOnly { get; set; }

        public bool Bedding { get; set; }

        public string TrainsIds { get; set; }

        public ICollection<TrainSearchesWagonType> TrainSearchesWagonTypes { get; set; }

        public TrainPriority TrainPriority { get; set; }

        public virtual ICollection<ReservedTicket> ReservedTickets { get; set; }


    }
}
