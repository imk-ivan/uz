﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicketSearcher.TrainSearchInfrastructure
{
    public class WagonPlace
    {
        public string WagonTypeId { get; set; }
        public int WagonNumber { get; set; }
        public int PlaceNumber { get; set; }
        public int Railway { get; set; }
        public string TrainId { get; set; }
        public string WagonClass { get; set; }
    }
}
