﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UZ.Enums
{
    public enum TrainPriority
    {
        [Display(Name = "Любое")]
        None,

        [Display(Name = "Отправляются раньше")]
        Earlier,

        [Display(Name = "Отправляются позже")]
        Later,

        [Display(Name = "Минимальное время в пути")]
        MinimalTime

    }
}
