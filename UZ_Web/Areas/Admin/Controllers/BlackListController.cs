﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.Web.Controllers;
using UZ.Web.Models;

namespace UZ.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "admin")]
    public class BlackListController : BaseController
    {
        protected IDBService _dBService;


        public BlackListController(IUZRequestService _UZRequestService, IDBService dBService)
            : base(_UZRequestService)
        {
            _dBService = dBService;
        }

        [Route("admin/blacklist/")]
        public IActionResult Index()
        {
            var model = _dBService.BlackListService.Storage.OrderBy(blockedItem => blockedItem.Id);

            return View(model);
        }

        [HttpGet("admin/blacklist/create")]
        public IActionResult Create()
        {
            BlockedTrainDTO model = new BlockedTrainDTO();
            return View(model);
        }

        [HttpPost("admin/blacklist/create")]
        public async Task<IActionResult> Create(BlockedTrainDTO model)
        {
            if (ModelState.IsValid)
            {
                model.BlockedDate = DateTime.Now;
                var resultId = await _dBService.BlackListService.AddAsync(model);
                if (resultId != 0)
                {
                    TempData["SuccessAdd"] = true;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("TrainId", "Не удалось добавить поезд в черный список. Пожалуйста, повторите попытку позже.");
                    return View(model);
                }
            }
            
            return View(model);
        }

        [HttpDelete("admin/blacklist/remove")]
        public async Task<IActionResult> RemoveItem(int id)
        {
            bool result = false;

            result = await _dBService.BlackListService.RemoveAsync(id);

            NotificationViewModel notificationModel = new NotificationViewModel(result, "Отлично!", "Элемент успешно удален из ЧС.",
                "Упс!", "При удалении возникли ошибки.");

            return PartialView("_Notification", notificationModel);
        }

    }
}