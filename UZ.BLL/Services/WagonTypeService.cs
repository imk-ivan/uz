﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.DAL;
using UZ.DAL.Models;

namespace UZ.BLL.Services
{
    public class WagonTypeService : IWagonTypeService
    {
        private ApplicationDbContext db;

        private readonly IMapper mapper;

        public WagonTypeService(ApplicationDbContext _db, IMapper _mapper)
        {
            db = _db;
            mapper = _mapper;
        }

        public async Task<bool> AddAsync(WagonTypeDto wagonType)
        {
            bool result = false;
            try
            {

                WagonType wg =  mapper.Map<WagonType>(wagonType);
                await db.AddAsync(wg);
                await db.SaveChangesAsync();
                result = true;
            }
            catch { }
            return result;
        }

        public async Task<List<WagonTypeDto>> GetAllAsync()
        {
            try
            {
                var source = await db.WagonTypes.ToListAsync();
                return mapper.Map<List<WagonType>, List<WagonTypeDto>>(source);
            }
            catch (Exception ex)
            {
                return new List<WagonTypeDto>();
            }
        }

        public List<WagonTypeDto> GetAll()
        {
            try
            {
                var source = db.WagonTypes.ToList();
                return mapper.Map<List<WagonType>, List<WagonTypeDto>>(source);
            }
            catch (Exception ex)
            {
                return new List<WagonTypeDto>();
            }
        }

        public void Dispose()
        {
            db.Dispose();
        }


    }
}
