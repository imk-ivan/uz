﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class AdvancedStrainSearch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainSearches_AspNetUsers_UserId",
                table: "TrainSearches");

            migrationBuilder.DropIndex(
                name: "IX_TrainSearches_UserId",
                table: "TrainSearches");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "TrainSearches",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "TrainSearches",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Bedding",
                table: "TrainSearches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "BottomOnly",
                table: "TrainSearches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "TrainSearches",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "TrainSearches",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "TrainSearches",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TrainSearches_ApplicationUserId",
                table: "TrainSearches",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TrainSearches_AspNetUsers_ApplicationUserId",
                table: "TrainSearches",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainSearches_AspNetUsers_ApplicationUserId",
                table: "TrainSearches");

            migrationBuilder.DropIndex(
                name: "IX_TrainSearches_ApplicationUserId",
                table: "TrainSearches");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "TrainSearches");

            migrationBuilder.DropColumn(
                name: "Bedding",
                table: "TrainSearches");

            migrationBuilder.DropColumn(
                name: "BottomOnly",
                table: "TrainSearches");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "TrainSearches");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "TrainSearches");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "TrainSearches");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "TrainSearches",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TrainSearches_UserId",
                table: "TrainSearches",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TrainSearches_AspNetUsers_UserId",
                table: "TrainSearches",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
