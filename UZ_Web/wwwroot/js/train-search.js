﻿$(function () {
    $("#SaveSearchForm").submit(function (e) {
        e.preventDefault();
        var form = $(this);

        var trainsChecked = true;
        if ($('.checked-trains input').length > 0) {
            $('.checked-trains .trains-error').addClass('hidden');
            trainsChecked = $('.checked-trains input:checked').length;

            if (!trainsChecked) {
                $('.checked-trains .trains-error').removeClass('hidden');
            }
        }

        $('.wagon-types .types-error').addClass('hidden');
        var wagonTypesChecked = $('.wagon-types input:checked').length;

        if (!wagonTypesChecked) {
            $('.wagon-types .types-error').removeClass('hidden');
        }
        if (!trainsChecked || !wagonTypesChecked) {
            return false;
        }

        if (form.valid()) {

            var formData = new FormData($(this)[0]);
            //formData.append("Search.WagonTypes", wagonTypes);

            $('.wagon-types input:checked').each(function (index) {
                formData.append((typeof formDataKeyName !== 'undefined' ? (formDataKeyName + ".") : "") + "WagonTypes[" + index + "].Id", $(this).val());
                formData.append((typeof formDataKeyName !== 'undefined' ? (formDataKeyName + ".") : "") + "WagonTypes[" + index + "].Title", $(this).data('title'));
            })

            var actionUrl = '/train/save-search'
            if ($('#searchId').length !== 0) {
                actionUrl = '/train/update-search'
            }

            var currentEl = $("input[type=submit]", $(this));
            currentEl.parents('.loader').addClass("active");
            $.ajax({
                url: actionUrl,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    console.log(data);
                    $("#Notifications").html(data).addClass("show");
                },
                complete: function () {
                    currentEl.parents('.loader').removeClass("active");
                }
            });


        }
    })

    $(document).on('click', '#open-advanced-search', function (e) {
        e.preventDefault();
        $('.advanced-search-block').slideToggle('fast');
        $(this).addClass('hidden');
    })
        .on('change', '.checked-trains input', function () {
            $('.checked-trains .trains-error').addClass('hidden');
        })
        .on('change', '.wagon-types input', function () {
            $('.wagon-types .types-error').addClass('hidden');
        })
    $(".phone-block input").inputmask("99-999-99-99");

})
