﻿using System;
using System.Collections.Generic;
using System.Text;
using UZ.EmailSender.Interfaces;

namespace UZ.EmailSender.Services
{
    public class EmailConfiguration : IEmailConfiguration
    {
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
