﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Models
{
    public class StationDTO
    {
        [JsonProperty("title")]
        public string Title { get; set; } = "";

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; } = 0;
    }
}
