﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Models;
using UZ.DAL.Models;
using UZ.Enums;

namespace UZ.BLL.Interfaces
{
    public interface ITrainSearchService : IDisposable
    {
        DbSet<TrainSearch> Storage { get; }

        Task<T> GetAsync<T>(int id)
            where T : BaseTrainSearchDTO;

        Task<List<T>> GetUserSearchesAsync<T>(string userId)
            where T : BaseTrainSearchDTO;

        Task<List<T>> GetAllSearchesAsync<T>(DateTime? date = null, bool onlyActive = true)
            where T : BaseTrainSearchDTO;
        Task<List<T>> GetAllSearchesAsync<T>(string query = "", SortState sortOrder = SortState.DateAsc)
            where T : BaseTrainSearchDTO;

        Task<int> AddAsync(TrainSearchDTO trainSearch);
        Task<int> AddAsync(AdvancedTrainSearchDTO trainSearch);
        Task<bool> UpdateAsync(TrainSearchDTO trainSearchDto);
        Task<bool> UpdateAsync(AdvancedTrainSearchDTO advancedTrainSearchDto);
        bool UpdateSearchStatus(int searchId, bool activeStatus = true);
        Task<bool> UpdateSearchStatusAsync(int searchId, bool activeStatus = true);
        Task<bool> DeleteAsync(int id);        
    }

}