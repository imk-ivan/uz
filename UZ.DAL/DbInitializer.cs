﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UZ.DAL.Models;

namespace UZ.DAL
{
    public class DbInitializer
    {
        public static async Task InitializeAsync(ApplicationDbContext db, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            string adminEmail = "admin@gmail.com";
            string userName = "admin";
            string password = "admin";
            if (await roleManager.FindByNameAsync("admin") == null)
            {
                await roleManager.CreateAsync(new ApplicationRole("admin"));
            }
            if (await roleManager.FindByNameAsync("user") == null)
            {
                await roleManager.CreateAsync(new ApplicationRole("user"));
            }
            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                ApplicationUser admin = new ApplicationUser { Email = adminEmail, UserName = userName };
                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin");
                }
            }

            if (!db.WagonTypes.Any())
            {
                List<WagonType> wagonTypes = new List<WagonType>() {
                    new WagonType { Id = "Л", Title = "Люкс" },
                    new WagonType { Id = "К", Title = "Купе" },
                    new WagonType { Id = "П", Title = "Плацкарт" },
                    new WagonType { Id = "С1", Title = "ИС-1"},
                    new WagonType { Id = "С2", Title = "ИС-2"}
                };
                await db.WagonTypes.AddRangeAsync(wagonTypes);
                db.SaveChanges();
            }
        }
    }
}
