﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class ReservedTickets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "TrainSearches",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ReservedTickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ord = table.Column<int>(nullable: false),
                    From = table.Column<int>(nullable: false),
                    To = table.Column<int>(nullable: false),
                    Train = table.Column<string>(nullable: true),
                    Date = table.Column<string>(nullable: true),
                    WagonNumber = table.Column<int>(nullable: false),
                    WagonClass = table.Column<string>(nullable: true),
                    WagonType = table.Column<string>(nullable: true),
                    WagonRailway = table.Column<int>(nullable: false),
                    Charline = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Bedding = table.Column<int>(nullable: false),
                    Child = table.Column<string>(nullable: true),
                    Student = table.Column<string>(nullable: true),
                    Reserve = table.Column<int>(nullable: false),
                    PlaceNumber = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    SessionId = table.Column<string>(nullable: true),
                    ReservedTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReservedTickets", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReservedTickets");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "TrainSearches");
        }
    }
}
