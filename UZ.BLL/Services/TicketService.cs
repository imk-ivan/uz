﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.DAL;
using UZ.DAL.Models;

namespace UZ.BLL.Services
{
    public class TicketService : ITicketService
    {
        private ApplicationDbContext db;

        private readonly IMapper mapper;

        public TicketService(ApplicationDbContext _db, IMapper _mapper)
        {
            db = _db;
            mapper = _mapper;
        }

        public DbSet<ReservedTicket> Storage
        {
            get
            {
                return db.ReservedTickets;
            }
        }

        public async Task<TicketDTO> GetAsync(int id)
        {
            ReservedTicket ticket = await db.ReservedTickets.FirstOrDefaultAsync(trainSearch => trainSearch.Id == id);
            if (ticket != null)
            {
                return mapper.Map<TicketDTO>(ticket);
            }
            return null;
        }

        public async Task<int> AddAsync(TicketDTO ticket)
        {
            int result = 0;
            try
            {
                ReservedTicket newTicket = mapper.Map<ReservedTicket>(ticket);
                await db.AddAsync(newTicket);
                await db.SaveChangesAsync();
                result = newTicket.Id;
            }
            catch { }
            return result;
        }

        public int Add(TicketDTO ticket)
        {
            int result = 0;
            try
            {
                ReservedTicket newTicket = mapper.Map<ReservedTicket>(ticket);
                db.Add(newTicket);
                db.SaveChanges();
                result = newTicket.Id;
            }
            catch { }
            return result;
        }

        public async Task<List<TicketDTO>> GetAllAsync(int? bookingMinutesPeriod = null, bool onlyActive = false)
        {
            try
            {
                IQueryable<ReservedTicket> sourceQuery = db.ReservedTickets.AsQueryable();

                if (bookingMinutesPeriod != null)
                    sourceQuery = sourceQuery.Where(ticket => (DateTime.Now - ticket.ReservedTime).TotalMinutes > bookingMinutesPeriod);
                if (onlyActive)
                    sourceQuery = sourceQuery.Where(ticket => ticket.Active == true);

                var source = await sourceQuery.ToListAsync();
                
                return mapper.Map<List<ReservedTicket>, List<TicketDTO>>(source);
            }
            catch (Exception ex)
            {
                return new List<TicketDTO>();
            }
        }

        public async Task<bool> UpdateAsync(TicketDTO ticket)
        {
            bool result = false;
            try
            {
                ReservedTicket existingTicket = await db.ReservedTickets.FirstOrDefaultAsync(t => t.Id == ticket.Id);

                mapper.Map(ticket, existingTicket);
                db.Set<ReservedTicket>().Attach(existingTicket);
                db.Entry(existingTicket).State = EntityState.Modified;
                await db.SaveChangesAsync();
                result = true;
            }
            catch(Exception ex)
            { }
            return result;
        }

        public bool Update(TicketDTO ticket)
        {
            bool result = false;
            try
            {
                ReservedTicket existingTicket = db.ReservedTickets.FirstOrDefault(t => t.Id == ticket.Id);
                mapper.Map(ticket, existingTicket);
                db.Set<ReservedTicket>().Attach(existingTicket);
                db.Entry(existingTicket).State = EntityState.Modified;

                db.SaveChanges();
                result = true;
            }
            catch(Exception ex) {

            }
            return result;
        }

        public async Task<bool> UpdateTicketStatusAsync(int ticketId, bool activeStatus = true)
        {
            bool result = false;

            ReservedTicket existingTicket = await db.ReservedTickets.FindAsync(ticketId);
           
            if (existingTicket != null)
            {
                try
                {
                    existingTicket.Active = activeStatus;
                    db.Set<ReservedTicket>().Attach(existingTicket);                    
                    await db.SaveChangesAsync();
                    result = true;
                }
                catch (Exception ex) { }
            }

            return result;
        }
        public async Task<bool> DeleteAsync(int id)
        {
            bool result = false;
            ReservedTicket reservedTicket = await db.ReservedTickets.FindAsync(id);
            if (reservedTicket != null)
            {
                try
                {
                    db.Remove(reservedTicket);
                    await db.SaveChangesAsync();
                    result = true;
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public void Dispose()
        {
            db.Dispose();
        }

    }
}
