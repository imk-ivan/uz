﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UZ.BLL.Interfaces;
using UZ.DAL;
using UZ.DAL.Models;

namespace UZ.BLL.Services
{
    public class UserService : IUserService
    {
        private ApplicationDbContext db;

        private readonly IMapper mapper;

        public UserService(ApplicationDbContext _db, IMapper _mapper)
        {
            db = _db;
            mapper = _mapper;
        }

        public DbSet<ApplicationUser> Storage
        {
            get
            {
                return db.Users;
            }
        }

        public async Task<List<string>> GetAdminEmails()
        {
            return await db.Users.Where(user => user.UserRoles.Any(ur => ur.Role.Name == "admin")).Select(user => user.Email).ToListAsync();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
