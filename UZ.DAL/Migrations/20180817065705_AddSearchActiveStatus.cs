﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UZ.DAL.Migrations
{
    public partial class AddSearchActiveStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "TrainSearches",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "TrainSearches");
        }
    }
}
