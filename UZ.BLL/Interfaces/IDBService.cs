﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UZ.BLL.Interfaces
{
    public interface IDBService : IDisposable
    {
        ITrainSearchService TrainSearchService { get; }
        IWagonTypeService WagonTypeService { get; }
        ITicketService TicketService { get; }
        IBlockedTrainService BlackListService { get; }
        IUserService UserService { get; }
    }
}
