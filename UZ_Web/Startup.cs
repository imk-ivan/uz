﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UZ.Web.Models;
using UZ.Web.Services;
using UZ.DAL;
using UZ.DAL.Models;
using UZ.BLL.Infrastructure.UZRequestModule.Configuration;
using UZ.BLL.Infrastructure.UZRequestModule;
using AutoMapper;
using UZ.BLL.Infrastructure.Mapping;
using UZ.BLL.Interfaces;
using UZ.BLL.Services;
using UZ.Web.Localization.Identity;
using UZ.EmailSender.Services;
using UZ.EmailSender.Interfaces;

namespace UZ_Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.User.RequireUniqueEmail = true;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<CustomIdentityErrorDescriber>();

            services.AddScoped(typeof(IDBService), typeof(DBService));

            // Add application services.
            services.AddMvc();

            services.AddAutoMapper(x => x.AddProfile(new MappingProfile()));

            services.AddSingleton<IEmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddSingleton<IUZRequestConfiguration>(Configuration.GetSection("UZRequestConfiguration").Get<UZRequestConfiguration>());
            services.AddTransient<IUZRequestService, UZRequestService>();

            services.AddSignalR();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // обработка ошибок HTTP
            app.UseStatusCodePagesWithReExecute("/error/errorstatus/{0}");

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=TrainSearch}/{action=Index}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
