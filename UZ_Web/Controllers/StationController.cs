﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Models;

namespace UZ.Web.Controllers
{
    public class StationController : BaseController
    {
        public StationController(IUZRequestService _uzService)
            : base(_uzService) {
        }

        [Route("/stations/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("/stationssearch")]
        public IActionResult StationSearch(string term)
        {
            List<StationDTO> stations = UZRequestService.GetStations(term);
            return Json(stations);
        }
    }
}