﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UZ.BLL.Models;
using UZ.Enums;

namespace UZ.Web.Areas.Models
{
    public class TrainSearchesIndexViewModel
    {
        public List<AdvancedTrainSearchDTO> Searches { get; set; }
        public SortViewModel SortModel { get; set; }
    }
}
