﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using UZ.BLL.Infrastructure.UZRequestModule.Configuration;
using UZ.BLL.Models;
using UZ.BLL.Utils;

namespace UZ.BLL.Infrastructure.UZRequestModule
{
    public class UZRequestService : IUZRequestService
    {
        //private const string API_BASE = "https://booking.uz.gov.ua/ru/";
        //private const string API_PAY_BASE = "https://ticket2.uz.gov.ua/ru/";
        RestClient client;
        RestClient payClient;

        private readonly IUZRequestConfiguration _requestConfig;


        public UZRequestService(IUZRequestConfiguration _config)
        {
            _requestConfig = _config;

            client = new RestClient(_requestConfig.API_BASE) { Encoding = Encoding.UTF8 };
            ICredentials credentials = new NetworkCredential("ikolpi", "HGvm4ZMD");
            client.Proxy = new WebProxy("91.108.64.141:29842", true, null, credentials);

            payClient = new RestClient(_requestConfig.API_PAY_BASE);
            payClient.Proxy = new WebProxy("91.108.64.141:29842", true, null, credentials);
        }

        public UZRequestService(IUZRequestConfiguration _config, ICredentials credentials, string proxyAddress)
        {
            _requestConfig = _config;

            client = new RestClient(_requestConfig.API_BASE) { Encoding = Encoding.UTF8 };
            payClient = new RestClient(_requestConfig.API_PAY_BASE);

            InitializeProxy(credentials, proxyAddress);
        }

        public void InitializeProxy(ICredentials credentials, string proxyAddress)
        {
            client.Proxy = new WebProxy(proxyAddress, true, null, credentials);
            payClient.Proxy = new WebProxy(proxyAddress, true, null, credentials);
        }

        public List<StationDTO> GetStations(string term, int stationCount = 5)
        {
            List<StationDTO> cityStations = new List<StationDTO>();
            string searchUrl = "train_search/station/";
            var request = new RestRequest(searchUrl, Method.GET);
            request.AddParameter("term", term);
            try
            {
                var result = client.Execute(request);
                cityStations = JsonConvert.DeserializeObject<List<StationDTO>>(result.Content);

            }
            catch (Exception ex)
            {

            }

            return cityStations.Take(stationCount).ToList();
        }

        public List<TrainSearchData> TrainSearch(TrainSearchDTO trainSearch)
        {
            return TrainSearch((BaseTrainSearchDTO)trainSearch);
        }

        public List<TrainSearchData> TrainSearch(AdvancedTrainSearchDTO trainSearch)
        {
            return TrainSearch((BaseTrainSearchDTO)trainSearch);
        }

        public List<TrainSearchData> TrainSearch(BaseTrainSearchDTO trainSearch)
        {
            List<TrainSearchData> trains = new List<TrainSearchData>();

            string searchUrl = "train_search/";
            var request = new RestRequest(searchUrl, Method.POST);

            request.AddParameter("from", trainSearch.From);
            request.AddParameter("to", trainSearch.To);
            request.AddParameter("date", trainSearch.Date.ToString("yyyy-MM-dd"));
            request.AddParameter("time", "00:00");

            try
            {
                var result = client.Execute(request);
                TrainSearchResponse searchResponse = JsonConvert.DeserializeObject<TrainSearchResponse>(result.Content);
                if(searchResponse != null 
                    && searchResponse.Error == 0 
                    && searchResponse.Data != null 
                    && searchResponse.Data.List != null
                    && searchResponse.Data.List.Count > 0
                    )
                {
                    trains = searchResponse.Data.List;
                }

            }
            catch (Exception ex)
            {

            }

            return trains;
        }
        public TrainWagonsResponse GetTrainWagons(int from, int to, string date, string train, string wagonTypeId, int getTpl = 1)
        {
            TrainWagonsResponse response = new TrainWagonsResponse();

            string searchUrl = "train_wagons/";
            var request = new RestRequest(searchUrl, Method.POST);
            request.Timeout = 5000;
            request.AddParameter("from", from);
            request.AddParameter("to", to);
            request.AddParameter("date", date);
            request.AddParameter("train", train);
            request.AddParameter("wagon_type_id", wagonTypeId);
            request.AddParameter("get_tpl", getTpl);
            string responseContent = "";
            try
            {
                client.ReadWriteTimeout = 5000;
                //Stopwatch sw = new Stopwatch();
                //sw.Start();
                client.Timeout = 5000;
                var result = client.Execute(request);
                //sw.Stop();
                //Console.WriteLine($"Request time - {sw.Elapsed}");
                responseContent = result.Content;
                //Console.WriteLine($"Train: {train}, response: {responseContent}");
                response = JsonConvert.DeserializeObject<TrainWagonsResponse>(result.Content);
                if (response.Error > 0)
                {
                    int z = 5;
                }
            }
            catch (Exception ex)
            {
                if(!responseContent.Contains("свободных мест"))
                {
                    //int y = 7;
                }
                else
                {
                    response.Error = 1;
                }
                
            }

            return response;

        }

        public WagonPlacesResponse GetWagonPlaces(int from, int to, string date, string train, string wagonTypeId, int wagonNum, string wagonClass)
        {
            WagonPlacesResponse result = null;

            string searchUrl = "train_wagon/";
            var request = new RestRequest(searchUrl, Method.POST);

                     

            request.AddParameter("from", from);
            request.AddParameter("to", to);
            request.AddParameter("date", date);
            request.AddParameter("train", train);
            request.AddParameter("wagon_type", wagonTypeId);
            request.AddParameter("wagon_num", wagonNum);
            request.AddParameter("wagon_class", wagonClass);            
            string cont = "";
            try
            {
                var requestResult = client.Execute(request);
                cont = requestResult.Content;
                result = JsonConvert.DeserializeObject<WagonPlacesResponse>(requestResult.Content);

            }
            catch (Exception ex)
            {
                if (!cont.Contains("свободных мест"))
                {
                    int y = 7;
                }
            }
            return result;
        }

        public string GetCart(string sessionId)
        {
            string cart = "";

            string cartUrl = "cart/";
            var request = new RestRequest(cartUrl, Method.GET);
            request.AddCookie("_gv_sessid", sessionId);
            //request.AddParameter("term", term);
            try
            {
                var result = client.Execute(request);
                cart = result.Content;

            }
            catch (Exception ex)
            {

            }
            return cart;
        }

        public TicketResponse AddTicketToCart(OrderDto order, out string sessionId, int? captcha = null)
        {
            if (order == null)
                throw new ArgumentNullException("Order cannot be a null");

            TicketResponse resultResponse = null;
            sessionId = "";

            string payUrl = "cart/add/";
            var request = new RestRequest(payUrl, Method.POST);
            //request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
            //request.AddCookie("_gv_sessid", "p89n4b9rt9jkqclhehfe3hp4r5");
            //request.AddParameter("HTTPSERVERID", "server2");
            //request.AddParameter("_ga", "GA1.3.703291561.1532442604");
            //request.AddParameter("_gid", "GA1.3.1841755613.1532442604");
            for (int i = 0; i < order.Places.Count; i++)
            {
                var place = order.Places[i];

                request.AddParameter($"places[{i}][ord]", place.Ord);
                request.AddParameter($"places[{i}][from]", place.From);
                request.AddParameter($"places[{i}][to]", place.To);
                request.AddParameter($"places[{i}][train]", place.Train);
                request.AddParameter($"places[{i}][date]", place.Date.ToString("yyyy-MM-dd"));
                request.AddParameter($"places[{i}][wagon_num]", place.WagonNumber);
                request.AddParameter($"places[{i}][wagon_class]", place.WagonClass);
                request.AddParameter($"places[{i}][wagon_type]", place.WagonType);
                request.AddParameter($"places[{i}][wagon_railway]", place.WagonRailway);
                request.AddParameter($"places[{i}][charline]", place.Charline);
                request.AddParameter($"places[{i}][firstname]", place.FirstName);
                request.AddParameter($"places[{i}][lastname]", place.LastName);
                request.AddParameter($"places[{i}][bedding]", place.Bedding);
                request.AddParameter($"places[{i}][student]", place.Student);
                request.AddParameter($"places[{i}][child]", place.Child);
                request.AddParameter($"places[{i}][reserve]", place.Reserve);
                request.AddParameter($"places[{i}][place_num]", place.PlaceNumber);
                if(captcha != null)
                {
                    request.AddParameter($"captcha", (int)captcha);
                }
                
                //if (i > 0)
                //    requestFormStr += "&";
                //requestFormStr += $"places[0][ord]={place.Ord}&places[0][from]={place.From}&places[0][to]={place.To}&places[0][train]={place.Train}&places[0][date]={place.Date}&places[0][wagon_num]={place.WagonNumber}&places[0][wagon_class]={place.WagonClass}&places[0][wagon_type]={place.WagonType}&places[0][wagon_railway={place.WagonRailway}&places[0][charline]={place.Charline}&places[0][firstname]={place.FirstName}&places[0][lastname]={place.LastName}&places[0][bedding]={place.Bedding}&places[0][reserve]={place.Reserve}&places[0][place_num]={place.PlaceNumber}";
            }

            try

            {
                var result = client.Execute(request);
                if (result.Content.Contains("\"captcha\":1"))
                {
                    resultResponse = new TicketResponse() { Error = 1, Captcha = 1, Data = null };
                }
                else
                {
                    resultResponse = JsonConvert.DeserializeObject<TicketResponse>(result.Content);   
                }
                sessionId = result.Cookies.FirstOrDefault(x => x.Name == "_gv_sessid")?.Value;

            }
            catch (Exception ex)
            {

            }

            return resultResponse;
        }

        public byte[] GetTicketCaptchaImage(string sessionId)
        {
            string result = "";
            Image img = null;
            byte[] captchaImgBytes = null;
            double captchaRequestValue = 0.706384741121985;

            string url = $"captcha/?{captchaRequestValue}";

            var request = new RestRequest(url, Method.GET);
            request.AddCookie("_gv_sessid", sessionId);

            try
            {
                //var response = client.Execute(request);
                captchaImgBytes = client.DownloadData(request);
                img = ImageUtils.BytesToImage(captchaImgBytes);

                Image image = new Bitmap(2000, 1024);
                image.Save(@"C:\test.gif");
                //var x = Ocr.Read(image);
                Bitmap btm = new Bitmap(140,60);

            }
            catch (Exception ex)
            {

            }

            return captchaImgBytes;
        }

        public RevokeResponse RevokeTicketFromCart(string sessionId, string[] ids)
        {
            RevokeResponse response = null;

            string url = "cart/revoke/";
            var request = new RestRequest(url, Method.POST);
            request.AddCookie("_gv_sessid", sessionId);

            request.AddParameter("auto_revoke", 0);
            for(int i = 0; i < ids.Length;i++)
            {
                string id = ids[i];
                request.AddParameter($"ids[{i}]", id);
            }

            //request.AddParameter("Application/Json", orderJson, ParameterType.RequestBody);
            try
            {
                var result = client.Execute(request);
                response = JsonConvert.DeserializeObject<RevokeResponse>(result.Content);
                sessionId = result.Cookies.FirstOrDefault(x => x.Name == "_gv_sessid")?.Value;

            }
            catch (Exception ex)
            {

            }

            return response;
        }

        public string PayCart(string sessionId, string email)
        {
            string res = "";

            string payUrl = "cart/pay/";
            var request = new RestRequest(payUrl, Method.POST);
            request.AddCookie("_gv_sessid", sessionId);
            request.AddParameter("email", email);
            request.AddParameter("is_confirmed", 1);
            try
            {
                var result = client.Execute(request);
                res = result.Content;

            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public string RedirectToPay(string sessionId, int merchant_id, int order_id, int amount, string date, string description, string sd, string version, string signature, int enable_mobile)
        {
            string res = "";

            string payUrl = "";
            var request = new RestRequest(payUrl, Method.POST);
            //request.AddCookie("_gv_sessid", sessionId);
            request.AddParameter("merchant_id", merchant_id);
            request.AddParameter("order_id", order_id);
            request.AddParameter("amount", amount);
            request.AddParameter("date", date);
            request.AddParameter("description", description);
            request.AddParameter("sd", sd);
            request.AddParameter("version", version);
            request.AddParameter("signature", signature);
            request.AddParameter("enable_mobile", enable_mobile);
            try
            {
                var result = payClient.Execute(request);
                res = result.Content;

            }
            catch (Exception ex)
            {

            }

            return res;
        }
    }
}
