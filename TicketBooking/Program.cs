﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UZ.BLL.Infrastructure.Mapping;
using UZ.BLL.Infrastructure.Proxy;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Infrastructure.UZRequestModule.Configuration;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.BLL.Services;
using UZ.BLL.Utils;
using UZ.DAL;

namespace TicketBooking
{
    class Program
    {
        public static ServiceProvider serviceProvider;

        public static IConfiguration Configuration;
        public static IDBService db;
        public static IMapper mapper;
        public static IUZRequestConfiguration requestConfig;
        public static IUZRequestService UZRequestService;
        public static readonly string API_BASE;
        public static readonly string API_PAY_BASE;
        public const int TIME_PERIOD = 3000;
        public const int BOOKING_MINUTES_PERIOD = 6;

        public static ProxyConfig proxyConfig;

        public const int ThreadCount = 10;



        static Program()
        {
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (String.IsNullOrWhiteSpace(environment))
                throw new ArgumentNullException("Environment not found in ASPNETCORE_ENVIRONMENT");

            //Console.WriteLine("Environment: {0}", environment);

            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(AppContext.BaseDirectory))
                .AddJsonFile("appsettings.json", optional: true);
            if (environment == "Development")
            {

                builder
                    .AddJsonFile(
                        Path.Combine(AppContext.BaseDirectory, string.Format("..{0}..{0}..{0}", Path.DirectorySeparatorChar), $"appsettings.{environment}.json"),
                        optional: true
                    );
            }
            else
            {
                builder
                    .AddJsonFile($"appsettings.{environment}.json", optional: false);
            }

            Configuration = builder.Build();

            // Add application services.
            serviceProvider = new ServiceCollection()
                .AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")))
                .AddScoped(typeof(IDBService), typeof(DBService))
                .AddAutoMapper(x => x.AddProfile(new MappingProfile()))
                .AddSingleton<IUZRequestConfiguration>(Configuration.GetSection("UZRequestConfiguration").Get<UZRequestConfiguration>())
                .AddTransient<IUZRequestService, UZRequestService>()
                .BuildServiceProvider();

            mapper = serviceProvider.GetService<IMapper>();

            db = GetDBService();
            
            UZRequestService = serviceProvider.GetService<IUZRequestService>();

            requestConfig = Configuration.GetSection("UZRequestConfiguration").Get<UZRequestConfiguration>();

            proxyConfig = new ProxyConfig(UZ.Enums.ProxyUsingType.Consistently);

            string personsPath = Path.Combine(AppContext.BaseDirectory, "Persons.txt");
        }

        public static async Task Main(string[] args)
        {
            while (true)
            {
                
                List<TicketDTO> tickets = await GetAllTickets();
                int ticketsCount = tickets.Count;
                if(ticketsCount > 0)
                {
                    Booking(tickets);
                }
                
                //for (int i = 0; i <= ticketsCount; i += ThreadCount)
                //{

                //    var currentTickets = tickets.Skip(i).Take(ThreadCount).ToList();
                //    Booking(currentTickets);
                //}
                ////db.Dispose();
                //Thread.Sleep(TIME_PERIOD);
            }
        }

        private static void Booking(List<TicketDTO> currentTickets)
        {

            //Parallel.ForEach(
            //          currentTickets,
            //          new ParallelOptions { MaxDegreeOfParallelism = ThreadCount },
            //          ticket => { RebookTicket(ticket); }
            //);
            foreach (var ticket in currentTickets) {
                RebookTicket(ticket);
            }
        }

        private static bool RebookTicket(TicketDTO ticket)
        {
            bool rebookResult = false;
            try
            {
                //IDBService newDbService = GetDBService();
                IUZRequestService uzService = new UZRequestService(requestConfig, proxyConfig.Credentials, proxyConfig.GetAddress());

                string cartHtml = uzService.GetCart(ticket.SessionId);
                List<string> reserveIds = StringUtils.GetReserveIdFromCartHtml(cartHtml);

                RevokeResponse revokeResult = UZRequestService.RevokeTicketFromCart(ticket.SessionId, reserveIds.ToArray());
                if (revokeResult != null && revokeResult.Error == 0)
                {
                    OrderDto order = new OrderDto( new List<TicketDTO>() { ticket });

                    TicketResponse result = UZRequestService.AddTicketToCart(order, out string sessionId);
                    if (result != null && result.Error == 0)
                    {
                        ticket.SessionId = sessionId;
                        ticket.ReservedTime = DateTime.Now;
                        rebookResult = true;
                        Console.WriteLine(sessionId);                     
                    }
                    else
                    {                       
                        ReinitializeSearch(ticket);
                    }                 
                }
                else
                {
                    ReinitializeSearch(ticket);
                }
                db.TicketService.Update(ticket);
                //newDbService.Dispose();
            }
            catch(Exception ex)
            {

            }
            return rebookResult;
        }

        private static async Task<List<TicketDTO>> GetAllTickets()
        {
            List<TicketDTO> tickets = new List<TicketDTO>();
            tickets = await db.TicketService.GetAllAsync(BOOKING_MINUTES_PERIOD, true);
            return tickets;
        }

        private static bool ReinitializeSearch(TicketDTO ticket)
        {
            bool result = false;
            try
            {
                ticket.Active = false;
                if (ticket.TrainSearchId != null)
                    db.TrainSearchService.UpdateSearchStatus((int)ticket.TrainSearchId, true);
                result = true;
            }
            catch(Exception ex) { }

            return result;
            
        }

        private static IDBService GetDBService()
        {
            IDBService newDb;
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            ApplicationDbContext newContext = new ApplicationDbContext(optionsBuilder.Options);
            newDb = new DBService(newContext, mapper);

            return newDb;
        }
    }
}
