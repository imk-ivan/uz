﻿$.fn.mvcgrid.lang = {
        text: {
        'contains': 'Содержит',
    'equals': 'Соответствует',
    'not-equals': 'Не соответствует',
    'starts-with': 'Начинается с',
    'ends-with': 'Заканчивается на'
    },
        number: {
            'equals': 'Равно',
    'not-equals': 'Не равно',
    'less-than': 'Меньше чем',
    'greater-than': 'Больше чем',
    'less-than-or-equal': 'Меньше или равно',
    'greater-than-o-requal': 'Больше или равно'
    },
        date: {
        'equals': 'Соответствует',
    'not-equals': 'Не соответствует',
    'earlier-than': 'Раньше чем',
    'later-than': 'Позже чем',
    'earlier-than-or-equal': 'Раньше или равно',
    'later-than-or-equal': 'Позже или равно'
    },
        enum: {
        'equals': 'Равно',
    'not-equals': 'Не равно',
    },
        filter: {
        'apply': '✔',
    'remove': '✘'
    },
        operator: {
        'select': '',
    'and': 'et',
    'or': 'ou'
    }
};