﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UZ.BLL.Models;
using UZ.Enums;

namespace UZ.Web.Areas.Models
{
    public class SortViewModel
    {
        public SortState EmailSort { get; set; }
        public SortState FirstNameSort { get; set; }
        public SortState LastNameSort { get; set; } 
        public SortState FromSort { get; set; } 
        public SortState ToSort { get; set; }     
        public SortState DateSort { get; set; }
        public SortState Current { get; set; }     // значение свойства, выбранного для сортировки
        public bool Up { get; set; }  // Сортировка по возрастанию или убыванию

        public SortViewModel(SortState sortOrder)
        {
            // значения по умолчанию
            EmailSort = SortState.EmailAsc;
            FirstNameSort = SortState.FirstNameAsc;
            LastNameSort = SortState.LastNameAsc;
            FromSort = SortState.FromAsc;
            ToSort = SortState.ToAsc;
            DateSort = SortState.DateDesc;
            Up = true;

            if (sortOrder == SortState.EmailDesc 
                || sortOrder == SortState.FirstNameDesc 
                || sortOrder == SortState.LastNameDesc
                || sortOrder == SortState.FromDesc
                || sortOrder == SortState.ToDesc
                || sortOrder == SortState.DateDesc)
            {
                Up = false;
            }

            switch (sortOrder)
            {
                case SortState.EmailDesc:
                    Current = EmailSort = SortState.EmailAsc;
                    break;
                case SortState.EmailAsc:
                    Current = EmailSort = SortState.EmailDesc;
                    break;
                case SortState.FirstNameAsc:
                    Current = FirstNameSort = SortState.FirstNameDesc;
                    break;
                case SortState.FirstNameDesc:
                    Current = FirstNameSort = SortState.FirstNameAsc;
                    break;
                case SortState.LastNameAsc:
                    Current = LastNameSort = SortState.LastNameDesc;
                    break;
                case SortState.LastNameDesc:
                    Current = LastNameSort = SortState.LastNameAsc;
                    break;
                case SortState.FromAsc:
                    Current = FromSort = SortState.FromDesc;
                    break;
                case SortState.FromDesc:
                    Current = FromSort = SortState.FromAsc;
                    break;
                case SortState.ToAsc:
                    Current = ToSort = SortState.ToDesc;
                    break;
                case SortState.ToDesc:
                    Current = ToSort = SortState.ToAsc;
                    break;
                case SortState.DateAsc:
                    Current = DateSort = SortState.DateDesc;
                    break;
                case SortState.DateDesc:
                    Current = DateSort = SortState.DateAsc;
                    break;
                default:
                    Current = DateSort = SortState.DateAsc;
                    break;
            }
        }
    }
}
