﻿$(function () {
    $(document).on('change', ".user-search input[type='checkbox']", function (e) {

        var activeStatus = $(this).is(':checked');
        var searchId = $(this).parent('.user-search').data('search-id');

        $.ajax({
            url: '/train/set-search-status',
            data: { searchId: searchId, status: activeStatus },
            type: 'POST',
            success: function (data) {
                console.log(data);
            }
        });

    })
})
