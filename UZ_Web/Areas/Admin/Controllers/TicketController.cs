﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UZ.BLL.Infrastructure.UZRequestModule;
using UZ.BLL.Interfaces;
using UZ.BLL.Models;
using UZ.Web.Controllers;
using UZ.Web.Models;

namespace UZ.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "admin")]
    public class TicketController : BaseController
    {
        protected IDBService _dBService;


        public TicketController(IUZRequestService _UZRequestService, IDBService dBService)
            : base(_UZRequestService)
        {
            _dBService = dBService;
        }

        [Route("admin/tickets/")]
        public IActionResult Index()
        {
            var model = _dBService.TicketService.Storage.OrderBy(ticket => ticket.Id);

            return View(model);
        }

        [Route("admin/ticket/{id}")]
        public async Task<IActionResult> Detail(int id)
        {
            TicketDTO model = await _dBService.TicketService.GetAsync(id);
            if(model == null)
            {
                return new NotFoundResult();
            }

            var ticketData = UZRequestService.GetWagonPlaces(model.From, model.To, model.Date.ToString("yyyy-MM-dd"), model.Train, model.WagonType, model.WagonNumber, model.WagonClass);

            return View(model);                
        }
                
        [HttpPost("admin/ticket/set-ticket-status")]
        public async Task<IActionResult> SetTicketStatus(int ticketId, bool status)
        {
            bool result = false;            
            
            result = await _dBService.TicketService.UpdateTicketStatusAsync(ticketId, status);

            NotificationViewModel notificationModel = new NotificationViewModel(result, "Отлично!", "Статус билета изменен.",
                "Упс!", "При изменении статуса возникли ошибки.");

            return PartialView("_Notification", notificationModel);
        }
        [HttpDelete("admin/ticket/remove")]
        public async Task<IActionResult> RemoveTicket(int id)
        {
            bool result = false;

            result = await _dBService.TicketService.DeleteAsync(id);

            NotificationViewModel notificationModel = new NotificationViewModel(result, "Отлично!", "Билет успешно удален.",
                "Упс!", "При удалении возникли ошибки.");

            return PartialView("_Notification", notificationModel);
        }
    }
}